/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import ConexionSQLDB.DataBaseConnect;
import java.io.IOException;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yoshi
 */
@WebServlet(name = "login", urlPatterns = {"/login"})
public class login extends HttpServlet {

    
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
                try {
                   
                String un=request.getParameter("user");
		String pw=request.getParameter("password");    
                
                Connection cnx = DataBaseConnect.getConnection();
                PreparedStatement pst = cnx.prepareStatement("Select user_name, pass from clientes where user_name=? and pass=?");

                
                
		pst.setString(1, un);
                pst.setString(2, pw);
                ResultSet rs = pst.executeQuery(); 
                
                if(rs.next()){
                response.sendRedirect("productos.jsp");
                } else{
                
                 if(un.equals("admin") && pw.equals("admin"))
		{
			response.sendRedirect("system.jsp");
			return;
		}
		else
		{
			response.sendRedirect("login.jsp");
			return;
		}
                }
                }catch (SQLException ex) {
            Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
       }