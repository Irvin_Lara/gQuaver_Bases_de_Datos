/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import ConexionSQLDB.DataBaseConnect;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yoshi
 */
@WebServlet(name = "accesorioagr", urlPatterns = {"/accesorioagr"})
public class accesorioagr extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet REGISTRO</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet REGISTRO at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */ 
    
    
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
        try {
           
            int idprod = Integer.parseInt(request.getParameter("idprod"));
            int idacc = Integer.parseInt(request.getParameter("idacc"));
            int exist = Integer.parseInt(request.getParameter("exist"));
            int prec = Integer.parseInt(request.getParameter("prec"));
            String nomacc = request.getParameter("nomacc");
            String descacc = request.getParameter("descacc");
            
            Connection cnx = DataBaseConnect.getConnection();
            Statement st = cnx.createStatement();
            ResultSet rs =  st.executeQuery
                (
                    "insert into productos values("+idprod+","+exist+","+prec+")");
            
           
            st.executeQuery
                (
                    "insert into accesorios values("+idprod+","+idacc+",'"+nomacc+"','"+descacc+"')");
            
            
        response.sendRedirect("system.jsp");

        } catch (SQLException ex) {
        Logger.getLogger(registro.class.getName()).log(Level.SEVERE, null, ex);
    }
        
    }
}