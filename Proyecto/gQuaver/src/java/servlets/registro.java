/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import ConexionSQLDB.DataBaseConnect;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yoshi
 */
@WebServlet(name = "registro", urlPatterns = {"/registro"})
public class registro extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet REGISTRO</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet REGISTRO at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */ 
    
    
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
        try {
            
            String usuario = request.getParameter("usuario");
            String contra = request.getParameter("contra");
            String email = request.getParameter("email");
            String nombre = request.getParameter("nombre");
            String calle = request.getParameter("calle");
            String colonia = request.getParameter("colonia");
            String numero = request.getParameter("numero");
            String estado = request.getParameter("estado");
            String municipio = request.getParameter("municipio");
            String tarjeta = request.getParameter("tarjeta");
            String clave = request.getParameter("clave");
            int id = 3;
            
            
            Connection cnx = DataBaseConnect.getConnection();
            Statement st = cnx.createStatement();
            ResultSet rs =  st.executeQuery
                (
                    "insert into clientes values('"+id+"','"+nombre+"','"+email+"','"+usuario+"','"+contra+"')");
            
           
            st.executeQuery
                (
                    "insert into direcciones values('"+id+"','"+id+"','"+calle+"','"+colonia+"','"+numero+"','"+estado+"','"+municipio+"')");
            
            st.executeQuery
                (
                    "insert into tarjetas_credito values('"+id+"','"+id+"','"+tarjeta+"','"+clave+"')");
            
        response.sendRedirect("login.jsp");

        } catch (SQLException ex) {
        Logger.getLogger(registro.class.getName()).log(Level.SEVERE, null, ex);
    }
        
    }
}