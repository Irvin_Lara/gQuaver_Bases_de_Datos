<%-- 
    Document   : productos
    Created on : 28-may-2017, 19:00:36
    Author     : tiiko
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
      <title>Catálogo de productos</title>
      <meta charset="utf-8">
      <!-- se vea bien en dispositivos móviles-->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!--Llamamos al archivo CSS -->
      <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
      <link rel="shortcut icon" type="image/x-icon" href="icons/icon.png" />
      <link rel="stylesheet" href="css/style.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div id="header">
         <nav class="navbar navbar-inverse navbar-fixed-top">
            <img class="icon" src="icons/icon.png" alt="icono guitarra">
            <div class="container-fluid">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  </button>
                  <a id="gQuaver" class="navbar-brand" href="#">Catálogo de productos</a>
               </div> 
                <a href="index.jsp"><button type="button" class="btn btn-default navbar-btn" id="myBtn"> Cerrar sesión</button></a>
             </div>
         </div>
        <div id="main">
            <div class="parallaxP">
                <div class="titulos">
                    <h1>Exprésate</h1>
                    <h2>Ya sea esuchando</h2>
                    <h2>música o creándola.</h2>
                </div>
                <div class="titulos2">
                    <h1>Consigue:</h1>
                    <h2>
                        <ul>
                            <li>Instrumentos</li>
                            <li>Álbumes</li>
                            <li>Cursos</li>
                        </ul>
                    </h2>
                </div>
            </div>
            <div class="productos">
                <h1>Instrumentos musicales</h1>
                <div class="contenedor">
                    <img clas="imag" src="images/mitchell.jpg"/>
                    <div>
                        <p>Mitchell</p>
                        <p>Guitarra eléctrica.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/gibson.jpg"/>
                    <div>
                        <p>Gibson</p>
                        <p>Guitarra eléctrica.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/rogueBajo.jpg"/>
                    <div>
                        <p>Rogue</p>
                        <p>Bajo eléctrico.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/mitchellA.jpg"/>
                    <div>
                        <p>Mitchell</p>
                        <p>Guitarra acústica.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/rogueA.jpg"/>
                    <div>
                        <p>Rogue</p>
                        <p>Guitarra acústica.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/alvarez.jpg"/>
                    <div>
                        <p>Álvarez</p>
                        <p>Guitarra </p><p>electro-acústica.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/luna.jpg"/>
                    <div>
                        <p>Luna</p>
                        <p>Guitarra </p><p>electro-acústica.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/lade.jpg"/>
                    <div>
                        <p>Lade</p>
                        <p>Saxofón.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/violinP.jpg"/>
                    <div>
                        <p>Antosvilla</p>
                        <p>Violín.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <br/>
                <h1>Accesorios, pedales y amplificadores</h1>
                <div class="contenedor">
                    <img clas="imag" src="images/baquetas.jpg"/>
                    <div>
                        <p>Percussion Labs</p>
                        <p>Baquetas.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/puas.jpg"/>
                    <div>
                        <p>Clayton</p><p>Garage Band</p>
                        <p>25 púas.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/perris.jpg"/>
                    <div>
                        <p>Perris</p>
                        <p>Guitar Strap.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="contenedor">
                    <img clas="imag" src="images/gear.jpg"/>
                    <div>
                        <p>Gear</p>
                        <p>Bolsa para </p><p>guitarra acústica.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/cable.jpg"/>
                    <div>
                        <p>Gear braided</p>
                        <p>Cable 1/4.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/afinador.jpg"/>
                    <div>
                        <p>Lanikal Clip</p>
                        <p>Afinar electrónico</p><p> de guitarra.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="contenedor">
                    <img clas="imag" src="images/markbass.jpg"/>
                    <div>
                        <p>Markbass </p><p>New York</p>
                        <p>Amplificador </p><p>para bajo.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/rogue.jpg"/>
                    <div>
                        <p>Rogue G10</p>
                        <p>Amplificador</p><p> para guitarra </p><p>eléctrica.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <br/>
               <div class="contenedor">
                    <img clas="imag" src="images/pedalbajo.jpg"/>
                    <div>
                        <p>Markbass </p><p>Compressore </p><p>Tube</p>
                        <p>Pedal para bajo.</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <br/>
                <h1>Álbumes</h1>
                <div class="contenedor">
                    <img clas="imag" src="images/humanz.jpg"/>
                    <div>
                        <p>Humanz</p>
                        <p>Gorillaz</p>
                        <p>Indie Rock</p>
                        <p>2017</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/once.jpg"/>
                    <div>
                        <p>Once</p>
                        <p>Nightwish</p>
                        <p>Metal sinfónico</p>
                        <p>2004</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/ram.jpg"/>
                    <div>
                        <p>RAM</p>
                        <p>Daft Punk</p>
                        <p>Electrónica</p>
                        <p>2013</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/black.jpg"/>
                    <div>
                        <p>The beggining</p>
                        <p>The Black Eyed Peas</p>
                        <p>Pop</p>
                        <p>2010</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/death.jpg"/>
                    <div>
                        <p>Death Magnetic</p>
                        <p>Metallica</p>
                        <p>Thrash Metal</p>
                        <p>2008</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/love.jpg"/>
                    <div>
                        <p>Love</p>
                        <p>The Beatles</p>
                        <p>Pop Rock</p>
                        <p>2006</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/gaia.jpg"/>
                    <div>
                        <p>Gaia</p>
                        <p>Mägo de Oz</p>
                        <p>Folk Metal</p>
                        <p>2003</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/gaia2.jpg"/>
                    <div>
                        <p>Gaia II</p>
                        <p>Mägo de Oz</p>
                        <p>Folk Metal</p>
                        <p>2005</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/distopia.jpg"/>
                    <div>
                        <p>Dystopia</p>
                        <p>Megadeth</p>
                        <p>Thrash Metal</p>
                        <p>2016</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/system.jpg"/>
                    <div>
                        <p>Hypotize</p>
                        <p>System of a Down</p>
                        <p>Nu Metal</p>
                        <p>2005</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/avanged.jpg"/>
                    <div>
                        <p>Nightmare</p>
                        <p>Avanged Sevenfold</p>
                        <p>Heavy Metal</p>
                        <p>2010</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
                <div class="contenedor">
                    <img clas="imag" src="images/elvis.jpg"/>
                    <div>
                        <p>ELV1S</p>
                        <p>Elvis Presley</p>
                        <p>Rock and Roll</p>
                        <p>2002</p>
                        <div class="btn alv2">
                            <p>+</p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="parallaxCursos">
                <div class="titulosC">
                    <h1>¿Aún estás aprendiendo?</h1>
                    <h2>Te ofrecemos los</h2>
                    <h2>mejores cursos.</h2>
                </div>
            </div>
            <div class="productos2">
                <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/armoniaC.jpg" alt="armonía musical">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="viewed">257 <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="viewed">3 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.3</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Armonía musical + </p>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/pianoC.jpg" alt="piano">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">433 <i class="glyphicon glyphicon-eye-open"></i></span>
                        <span data-toggle="tooltip" title="Favorited">4 <i class="glyphicon glyphicon-star"></i></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.3</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Piano y teclado + </p>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/acusticaC.jpg" alt="guitara acústica">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">2.1K <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="Favorited">13 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.3</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Guitarra acústica + </p>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/eletricaC.jpg" alt="guitarra eléctrica">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">11.5K <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="Favorited">24 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.3</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Guitarra eléctrica + </p>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/bajoC.jpg" alt="bajo eléctrico">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">7.3K <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="Favorited">31 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.3</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Bajo eléctrico + </p>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/saxofonC.jpg" alt="saxofón">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">2.5K <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="Favorited">18 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.0</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Saxofón + </p>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/bateriaC.jpg" alt="bateria">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">1.5K <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="Favorited">20 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.0</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Batería + </p>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/violinC.jpg" alt="violin">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">5.5K <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="Favorited">25 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.0</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Violín + </p>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/cantoC.jpg" alt="canto">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">2.0K <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="Favorited">14 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.0</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Canto + </p>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
            </div>
        </div>
      <div id="footer">
      <div class="container-fluid bg-primary py-3">
         <div class="container">
            <div class="row py-3">
               <div class="col-md-9">
                  <p class="text-white">Copyright (c) gQuaver 2017 Todos los derechos reservados.</p>
               </div>
               <div class="col-md-3">
                  <div class="d-inline-block">
                     <div class="bg-circle-outline d-inline-block">
                        <a href="https://www.facebook.com/" class="text-white"><i class="fa fa-2x fa-fw fa-facebook"></i></a>
                     </div>
                     <div class="bg-circle-outline d-inline-block">
                        <a href="https://twitter.com/" class="text-white">
                        <i class="fa fa-2x fa-fw fa-twitter"></i></a>
                     </div>
                     <div class="bg-circle-outline d-inline-block">
                        <a href="https://www.linkedin.com/company/" class="text-white">
                        <i class="fa fa-2x fa-fw fa-linkedin"></i></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div>
            <!-- Bootsrap necesita jQuery para funcionar -->
      <script type="text/javascript" src="js/jquery-3.2.0.min.js"></script>
      <!-- Llamamos al JavaScript de Bootstrap-->
      <script src="js/bootstrap.min.js"></script>
    </body>
</html>
