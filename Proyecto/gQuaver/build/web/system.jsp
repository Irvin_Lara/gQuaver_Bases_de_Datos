<%-- 
    Document   : System
    Created on : 27-may-2017, 17:38:26
    Author     : Tiiko Papas
--%>
<%@page import="ConexionSQLDB.DataBaseConnect"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page language="java" %>
<%@ page import = "java.sql.Connection"%>
<%@ page import = "java.sql.DriverManager"%> 
<%@ page import = "java.sql.ResultSet"%> 
<%@ page import = "java.sql.Statement"%> 

<!DOCTYPE html>
<html>
   <head>
      <title>system</title>
      <meta charset="utf-8">
      <!-- se vea bien en dispositivos móviles-->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!--Llamamos al archivo CSS -->
      <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
      <link rel="shortcut icon" type="image/x-icon" href="icons/icon.png" />
      <link rel="stylesheet" href="css/style.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
   </head>
   <body>
     <div class="todo">
      <div id="header">
         <nav class="navbar navbar-inverse navbar-fixed-top">
            <img class="icon" src="icons/icon.png" alt="icono guitarra">
            <div class="container-fluid">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  </button>
                  <a id="gQuaver" class="navbar-brand" href="#">ADMINISTRACIÓN DE LA BASE DE DATOS</a>
               </div> 
                <a href="index.jsp"><button type="button" class="btn btn-default navbar-btn" id="myBtn"> Cerrar sesión</button></a>
             </div>
         </div>
         <div class="main">
             <div class="parallaxTrans">
                 <div id="section4" class="container-fluid">
                   <b><h1>IMPORTANTE</h1></b>
               </div>
             </div>
             <div class="tablas">
                 <b><p>Sólo el administrador del sitio puede ver esta información de carácter delicado.</p>
                 <p>Hacer mal uso de esta información se motvo de sanción por el código penal que
                     puedes consultar <a href="http://www.ordenjuridico.gob.mx/Congreso/2doCongresoNac/pdf/PinaLibien.pdf" target="_blank">aquí</a></p></b>
             </div>
             <div class="tablas">
                 <h1> CLIENTES </h1>
                 <table class="table table-hover">
                     <thead>
                         <tr>
                             <th>ID</th>
                             <th>Nombre</th>
                             <th>Email</th>
                             <th>Usuario</th>
                             <th>Pass</th>
                         </tr>
                     </thead>
                     <tbody>
                         <%
                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from clientes order by idCliente");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("<td>" + rs.getString(5) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        %>
                     </tbody>
                 </table>
         </div>
         <div class="tablas">
                 <h1> DIRECCIONES </h1>
                 <table class="table table-hover">
                     <thead>
                         <tr>
                             <th>ID Cliente</th>
                             <th>ID Dirección</th>
                             <th>Calle</th>
                             <th>Colonia</th>
                             <th>Número de casa</th>
                             <th>Estado</th>
                             <th>Municipo</th>
                         </tr>
                     </thead>
                     <tbody>
                         <%
                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from direcciones order by idCliente");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("<td>" + rs.getString(5) + "</td>");
                        out.println("<td>" + rs.getString(6) + "</td>");
                        out.println("<td>" + rs.getString(7) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        %>
                     </tbody>
                 </table>
         </div>
         <div class="tablas">
                 <h1> CURSOS </h1>
                 <table class="table table-hover">
                     <thead>
                         <tr>
                             <th>ID</th>
                             <th>Nombre</th>
                             <th>Descripción</th>
                             <th>Duración</th>
                         </tr>
                     </thead>
                     <tbody>
                         <%
                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from cursos order by idCurso");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        %>
                     </tbody>
                 </table>
                 <center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal4">Agregar</button></center><br/>
                 <center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal9">Borrar</button></center>
         </div>     
         <div class="tablas">
                 <h1> INSCRIPCIONES </h1>
                 <table class="table table-hover">
                     <thead>
                         <tr>
                             <th>ID Cliente</th>
                             <th>ID Curso</th>
                             <th>Fecha de incripción</th>
                         </tr>
                     </thead>
                     <tbody>
                         <%
                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from inscripciones order by idCliente");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getDate(3) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        %>
                     </tbody>
                 </table>
         </div>     
         <div class="tablas">
                 <h1> TARJETAS DE CREDITO </h1>
                 <table class="table table-hover">
                     <thead>
                         <tr>
                             <th>ID Cliente</th>
                             <th>ID Tarjeta</th>
                             <th>Número de tarjeta</th>
                             <th>Código de seguridad</th>
                         </tr>
                     </thead>
                     <tbody>
                         <%
                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from tarjetas_credito order by idCliente");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        %>
                     </tbody>
                 </table>
         </div>   
         <div class="tablas">
                 <h1> TRANSPORTISTAS </h1>
                 <table class="table table-hover">
                     <thead>
                         <tr>
                             <th>ID</th>
                             <th>Nombre</th>
                         </tr>
                     </thead>
                     <tbody>
                         <%
                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from transportista order by idTransportista");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        %>
                     </tbody>
                 </table>
                 <center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal5">Agregar</button></center><br/>
                 <center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal10">Borrar</button></center>
         </div>
         <div class="tablas">
                 <h1> PRODUCTOS </h1>
                 <table class="table table-hover">
                     <thead>
                         <tr>
                             <th>ID</th>
                             <th>Existencia</th>
                             <th>Precio</th>
                         </tr>
                     </thead>
                     <tbody>
                         <%
                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from productos order by idProducto");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + "$ " + rs.getString(3) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        %>
                     </tbody>
                 </table>
         </div>
         <div class="tablas">
                 <h1> INSTRUMENTOS </h1>
                 <table class="table table-hover">
                     <thead>
                         <tr>
                             <th>ID Producto</th>
                             <th>ID Instrumento</th>
                             <th>Nombre</th>
                             <th>Descripción</th>
                         </tr>
                     </thead>
                     <tbody>
                         <%
                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from instrumentos order by idProducto");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        %>
                     </tbody>
                 </table>
                 <center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Agregar</button></center><br/>
                 <center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal6">Borrar</button></center>
         </div>
         <div class="tablas">
                 <h1> ALBUMES </h1>
                 <table class="table table-hover">
                     <thead>
                         <tr>
                             <th>ID Producto</th>
                             <th>ID Album</th>
                             <th>Nombre</th>
                             <th>Artista</th>
                             <th>Género</th>
                             <th>Año</th>
                         </tr>
                     </thead>
                     <tbody>
                         <%
                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from albumes order by idProducto");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("<td>" + rs.getString(5) + "</td>");
                        out.println("<td>" + rs.getString(6) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        %>
                     </tbody>
                 </table>
                     <center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal2">Agregar</button></center><br/>
                     <center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal7">Borrar</button></center>
         </div>  
         <div class="tablas">
                 <h1> ACCESORIOS </h1>
                 <table class="table table-hover">
                     <thead>
                         <tr>
                             <th>ID Producto</th>
                             <th>ID Accesorio</th>
                             <th>Nombre</th>
                             <th>Descripción</th>
                         </tr>
                     </thead>
                     <tbody>
                         <%
                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from accesorios order by idProducto");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        %>
                     </tbody>
                 </table>
                 <center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal3">Agregar</button></center><br/>
                 <center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal8">Borrar</button></center>
         </div>
         <div class="tablas">
                 <h1> COMPRAS </h1>
                 <table class="table table-hover">
                     <thead>
                         <tr>
                             <th>ID Compra</th>
                             <th>ID Cliente</th>
                             <th>ID Producto</th>
                             <th>Cantidad</th>
                             <th>Subtotal</th>
                             <th>Iva</th>
                             <th>Total</th>
                             <th>Fecha de llegada</th>
                             <th>ID Tarjeta</th>
                             <th>ID Dirección</th>
                             <th>ID Transportista</th>
                         </tr>
                     </thead>
                     <tbody>
                         <%
                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from compras order by idCompra");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("<td>" + rs.getString(5) + "</td>");
                        out.println("<td>" + rs.getString(6) + "</td>");
                        out.println("<td>" + rs.getString(7) + "</td>");
                        out.println("<td>" + rs.getDate(8) + "</td>");
                        out.println("<td>" + rs.getString(9) + "</td>");
                        out.println("<td>" + rs.getString(10) + "</td>");
                        out.println("<td>" + rs.getString(11) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        %>
                     </tbody>
                 </table>
         </div>
     </div>
     
      <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar instrumento</h4>
      </div>
      <div class="modal-body">
          <form action='instrumentoagr' method='post'>
            <label>ID Producto</label><input type='text' class='form-control'  name='idprod' placeholder='Ingresa el ID del producto'><br/>
            <label>ID Instrumento</label><input type='text' class='form-control' name='idins' placeholder='Ingresa el ID del instrumento'><br/>
            <label>Existencia</label><input type='text' class='form-control'  name='exist' placeholder='Ingresa la existencia del producto'><br/>
            <label>Precio</label><input type='text' class='form-control'  name='prec' placeholder='Ingresa el precio del producto'><br/>
            <label>Nombre</label><input type='text' class='form-control'  name='nomins' placeholder='Ingresa el nombre del instrumento'><br/>
            <label>Descripción</label><input type='text' class='form-control' name='descins' placeholder='Ingresa la descripción del instrumento '><br/>
            <input type='submit' value='Agregar instrumento' class='btn btn-success btn-block'/>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
      
 <!-- Modal -->
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar album</h4>
      </div>
      <div class="modal-body">
          <form action='albumagr' method='post'>
            <label>ID Producto</label><input type='text' class='form-control'  name='idprod' placeholder='Ingresa el ID del producto'><br/>
            <label>ID Album</label><input type='text' class='form-control' name='idalb' placeholder='Ingresa el ID del album'><br/>
            <label>Existencia</label><input type='text' class='form-control'  name='exist' placeholder='Ingresa la existencia del producto'><br/>
            <label>Precio</label><input type='text' class='form-control'  name='prec' placeholder='Ingresa el precio del producto'><br/>
            <label>Nombre</label><input type='text' class='form-control'  name='nomalb' placeholder='Ingresa el nombre del album'><br/>
            <label>Artista</label><input type='text' class='form-control' name='artis' placeholder='Ingresa el artista o grupo'><br/>
            <label>Género</label><input type='text' class='form-control' name='gene' placeholder='Ingresa el género'><br/>
            <label>Año</label><input type='text' class='form-control' name='anio' placeholder='Ingresa el año de publicación'><br/>
            <input type='submit' value='Agregar album' class='btn btn-success btn-block'/>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
 
 <!-- Modal -->
<div id="myModal3" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar accesorio</h4>
      </div>
      <div class="modal-body">
          <form action='accesorioagr' method='post'>
            <label>ID Producto</label><input type='text' class='form-control'  name='idprod' placeholder='Ingresa el ID del producto'><br/>
            <label>ID Accesorio</label><input type='text' class='form-control' name='idacc' placeholder='Ingresa el ID del accesorio'><br/>
            <label>Existencia</label><input type='text' class='form-control'  name='exist' placeholder='Ingresa la existencia del producto'><br/>
            <label>Precio</label><input type='text' class='form-control'  name='prec' placeholder='Ingresa el precio del producto'><br/>
            <label>Nombre</label><input type='text' class='form-control'  name='nomacc' placeholder='Ingresa el nombre del accesorio'><br/>
            <label>Descripción</label><input type='text' class='form-control' name='descacc' placeholder='Ingresa la descripción del accesorio'><br/>
            <input type='submit' value='Agregar accesorio' class='btn btn-success btn-block'/>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
 
 <!-- Modal -->
<div id="myModal4" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar curso</h4>
      </div>
      <div class="modal-body">
          <form action='cursosagr' method='post'>
            <label>ID Curso</label><input type='text' class='form-control'  name='idcur' placeholder='Ingresa el ID del curso'><br/>
            <label>Nombre</label><input type='text' class='form-control'  name='nomcur' placeholder='Ingresa el nombre del curso'><br/>
            <label>Descripción</label><input type='text' class='form-control' name='desccur' placeholder='Ingresa la descripción del curso'><br/>
            <label>Duración</label><input type='text' class='form-control' name='dura' placeholder='Ingresa la duración del curso'><br/>
            <input type='submit' value='Agregar curso' class='btn btn-success btn-block'/>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>  
 
<!-- Modal -->
<div id="myModal5" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar transportista</h4>
      </div>
      <div class="modal-body">
          <form action='transportistaagr' method='post'>
            <label>ID Transportista</label><input type='text' class='form-control'  name='idtrans' placeholder='Ingresa el ID del transportista'><br/>
            <label>Nombre</label><input type='text' class='form-control'  name='nomtrans' placeholder='Ingresa el nombre del transportista'><br/>
            <input type='submit' value='Agregar transportista' class='btn btn-success btn-block'/>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
 
<!-- Modal -->
<div id="myModal6" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Borrar instrumento</h4>
      </div>
      <div class="modal-body">
          <form action='instrumentobor' method='post'>
            <label>ID Producto</label><input type='text' class='form-control'  name='idprod' placeholder='Ingresa el ID del producto'><br/>
            <input type='submit' value='Borrar instrumento' class='btn btn-success btn-block'/>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal7" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Borrar album</h4>
      </div>
      <div class="modal-body">
          <form action='albumbor' method='post'>
            <label>ID Producto</label><input type='text' class='form-control'  name='idprod' placeholder='Ingresa el ID del producto'><br/>
            <input type='submit' value='Borrar album' class='btn btn-success btn-block'/>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal8" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Borrar accesorio</h4>
      </div>
      <div class="modal-body">
          <form action='accesoriobor' method='post'>
            <label>ID Producto</label><input type='text' class='form-control'  name='idprod' placeholder='Ingresa el ID del producto'><br/>
            <input type='submit' value='Borrar accesorio' class='btn btn-success btn-block'/>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal9" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Borrar curso</h4>
      </div>
      <div class="modal-body">
          <form action='cursobor' method='post'>
            <label>ID Curso</label><input type='text' class='form-control'  name='idcur' placeholder='Ingresa el ID del curso'><br/>
            <input type='submit' value='Borrar curso' class='btn btn-success btn-block'/>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal10" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Borrar transportista</h4>
      </div>
      <div class="modal-body">
          <form action='transportistabor' method='post'>
            <label>ID Transportista</label><input type='text' class='form-control'  name='idtrans' placeholder='Ingresa el ID del transportista'><br/>
            <input type='submit' value='Borrar transportista' class='btn btn-success btn-block'/>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

    <!-- Bootsrap necesita jQuery para funcionar -->
      <script type="text/javascript" src="js/jquery-3.2.0.min.js"></script>
      <!-- Llamamos al JavaScript de Bootstrap-->
      <script src="js/bootstrap.min.js"></script>
                   
     </body>
</html>
