package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("      <title>Login</title>\n");
      out.write("      <meta charset=\"utf-8\">\n");
      out.write("      <!-- se vea bien en dispositivos móviles-->\n");
      out.write("      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("      <!--Llamamos al archivo CSS -->\n");
      out.write("      <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\">\n");
      out.write("      <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"icons/icon.png\" />\n");
      out.write("      <link rel=\"stylesheet\" href=\"css/style.css\">\n");
      out.write("      <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("      <div id=\"header\">\n");
      out.write("         <nav class=\"navbar navbar-inverse navbar-fixed-top\">\n");
      out.write("            <img class=\"icon\" src=\"icons/icon.png\" alt=\"icono guitarra\">\n");
      out.write("            <div class=\"container-fluid\">\n");
      out.write("               <div class=\"navbar-header\">\n");
      out.write("                  <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#myNavbar\">\n");
      out.write("                  <span class=\"icon-bar\"></span>\n");
      out.write("                  </button>\n");
      out.write("                  <a id=\"gQuaver\" class=\"navbar-brand\" href=\"#\">gQuaver</a>\n");
      out.write("               </div>\n");
      out.write("            </div>\n");
      out.write("         </nav>\n");
      out.write("      </div>\n");
      out.write("        <div id=\"mainLog\">\n");
      out.write("            <div class=\"parallaxLog\">\n");
      out.write("                 <div id=\"section4\" class=\"container-fluid\">\n");
      out.write("                   <b><h1>Iniciar sesión</h1></b>\n");
      out.write("               </div>\n");
      out.write("            <div class=\"login\">\n");
      out.write("                 <!-- Modal -->\n");
      out.write("               <div class=\"modal-header\" style=\"padding:35px 50px;\">\n");
      out.write("                  <h4><span class=\"glyphicon glyphicon-lock\"></span> Inicia sesión</h4>\n");
      out.write("               </div>\n");
      out.write("               <div class=\"modal-body\" style=\"padding:40px 50px;\">\n");
      out.write("                   <form class=\"hola\" action=\"login\" method=\"post\">\n");
      out.write("                     <div class=\"form-group\">\n");
      out.write("                        <label for=\"usrname\"><span class=\"glyphicon glyphicon-user\"></span> Usuario</label>\n");
      out.write("                        <input type=\"text\" class=\"form-control\" id=\"usrname\" name=\"user\" placeholder=\"Ingresa tu email\">\n");
      out.write("                     </div>\n");
      out.write("                     <div class=\"form-group\">\n");
      out.write("                        <label for=\"psw\"><span class=\"glyphicon glyphicon-eye-open\"></span> Contraseña</label>\n");
      out.write("                        <input type=\"password\" class=\"form-control\" id=\"psw\" name=\"password\"  placeholder=\"Ingresa contaseña\">\n");
      out.write("                     </div>\n");
      out.write("                      <input type=\"submit\" value=\"Iniciar sesión\" id=\"iniNaranja\" class=\"btn btn-success btn-block\" />\n");
      out.write("                  </form>\n");
      out.write("                  <div class=\"modal-footer\">\n");
      out.write("                      <a href=\"index.jsp\"><button type=\"submit\" class=\"btn btn-danger btn-default cancelar\" data-dismiss=\"modal\"><span class=\"glyphicon glyphicon-remove\"></span> Cancelar</button></a>\n");
      out.write("                      <div id=\"reg\" class=\"registrar btn\">¡Regístrate aquí!</div>\n");
      out.write("                 </div>\n");
      out.write("               </div>\n");
      out.write("            </div>\n");
      out.write("            <br/>\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("      <div id=\"footer\">\n");
      out.write("      <div class=\"container-fluid bg-primary py-3\">\n");
      out.write("         <div class=\"container\">\n");
      out.write("            <div class=\"row py-3\">\n");
      out.write("               <div class=\"col-md-9\">\n");
      out.write("                  <p class=\"text-white\">Copyright (c) gQuaver 2017 Todos los derechos reservados.</p>\n");
      out.write("               </div>\n");
      out.write("               <div class=\"col-md-3\">\n");
      out.write("                  <div class=\"d-inline-block\">\n");
      out.write("                     <div class=\"bg-circle-outline d-inline-block\">\n");
      out.write("                        <a href=\"https://www.facebook.com/\" class=\"text-white\"><i class=\"fa fa-2x fa-fw fa-facebook\"></i></a>\n");
      out.write("                     </div>\n");
      out.write("                     <div class=\"bg-circle-outline d-inline-block\">\n");
      out.write("                        <a href=\"https://twitter.com/\" class=\"text-white\">\n");
      out.write("                        <i class=\"fa fa-2x fa-fw fa-twitter\"></i></a>\n");
      out.write("                     </div>\n");
      out.write("                     <div class=\"bg-circle-outline d-inline-block\">\n");
      out.write("                        <a href=\"https://www.linkedin.com/company/\" class=\"text-white\">\n");
      out.write("                        <i class=\"fa fa-2x fa-fw fa-linkedin\"></i></a>\n");
      out.write("                     </div>\n");
      out.write("                  </div>\n");
      out.write("               </div>\n");
      out.write("            </div>\n");
      out.write("         </div>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("      <!-- Bootsrap necesita jQuery para funcionar -->\n");
      out.write("      <script type=\"text/javascript\" src=\"js/jquery-3.2.0.min.js\"></script>\n");
      out.write("      <script type=\"text/javascript\" src=\"js/script.js\"></script>\n");
      out.write("      <!-- Llamamos al JavaScript de Bootstrap-->\n");
      out.write("      <script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
