package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import ConexionSQLDB.DataBaseConnect;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public final class system_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write(" \n");
      out.write(" \n");
      out.write(" \n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("   <head>\n");
      out.write("      <title>system</title>\n");
      out.write("      <meta charset=\"utf-8\">\n");
      out.write("      <!-- se vea bien en dispositivos móviles-->\n");
      out.write("      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("      <!--Llamamos al archivo CSS -->\n");
      out.write("      <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\">\n");
      out.write("      <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"icons/icon.png\" />\n");
      out.write("      <link rel=\"stylesheet\" href=\"css/style.css\">\n");
      out.write("      <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">\n");
      out.write("   </head>\n");
      out.write("   <body>\n");
      out.write("     <div class=\"todo\">\n");
      out.write("      <div id=\"header\">\n");
      out.write("         <nav class=\"navbar navbar-inverse navbar-fixed-top\">\n");
      out.write("            <img class=\"icon\" src=\"icons/icon.png\" alt=\"icono guitarra\">\n");
      out.write("            <div class=\"container-fluid\">\n");
      out.write("               <div class=\"navbar-header\">\n");
      out.write("                  <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#myNavbar\">\n");
      out.write("                  <span class=\"icon-bar\"></span>\n");
      out.write("                  </button>\n");
      out.write("                  <a id=\"gQuaver\" class=\"navbar-brand\" href=\"#\">ADMINISTRACIÓN DE LA BASE DE DATOS</a>\n");
      out.write("               </div> \n");
      out.write("                <a href=\"index.jsp\"><button type=\"button\" class=\"btn btn-default navbar-btn\" id=\"myBtn\"> Cerrar sesión</button></a>\n");
      out.write("             </div>\n");
      out.write("         </div>\n");
      out.write("         <div class=\"main\">\n");
      out.write("             <div class=\"parallaxTrans\">\n");
      out.write("                 <div id=\"section4\" class=\"container-fluid\">\n");
      out.write("                   <b><h1>IMPORTANTE</h1></b>\n");
      out.write("               </div>\n");
      out.write("             </div>\n");
      out.write("             <div class=\"tablas\">\n");
      out.write("                 <b><p>Sólo el administrador del sitio puede ver esta información de carácter delicado.</p>\n");
      out.write("                 <p>Hacer mal uso de esta información se motvo de sanción por el código penal que\n");
      out.write("                     puedes consultar <a href=\"http://www.ordenjuridico.gob.mx/Congreso/2doCongresoNac/pdf/PinaLibien.pdf\" target=\"_blank\">aquí</a></p></b>\n");
      out.write("             </div>\n");
      out.write("             <div class=\"tablas\">\n");
      out.write("                 <h1> CLIENTES </h1>\n");
      out.write("                 <table class=\"table table-hover\">\n");
      out.write("                     <thead>\n");
      out.write("                         <tr>\n");
      out.write("                             <th>ID</th>\n");
      out.write("                             <th>Nombre</th>\n");
      out.write("                             <th>Email</th>\n");
      out.write("                             <th>Usuario</th>\n");
      out.write("                             <th>Pass</th>\n");
      out.write("                         </tr>\n");
      out.write("                     </thead>\n");
      out.write("                     <tbody>\n");
      out.write("                         ");

                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from clientes order by idCliente");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("<td>" + rs.getString(5) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        
      out.write("\n");
      out.write("                     </tbody>\n");
      out.write("                 </table>\n");
      out.write("         </div>\n");
      out.write("         <div class=\"tablas\">\n");
      out.write("                 <h1> DIRECCIONES </h1>\n");
      out.write("                 <table class=\"table table-hover\">\n");
      out.write("                     <thead>\n");
      out.write("                         <tr>\n");
      out.write("                             <th>ID Cliente</th>\n");
      out.write("                             <th>ID Dirección</th>\n");
      out.write("                             <th>Calle</th>\n");
      out.write("                             <th>Colonia</th>\n");
      out.write("                             <th>Número de casa</th>\n");
      out.write("                             <th>Estado</th>\n");
      out.write("                             <th>Municipo</th>\n");
      out.write("                         </tr>\n");
      out.write("                     </thead>\n");
      out.write("                     <tbody>\n");
      out.write("                         ");

                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from direcciones order by idCliente");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("<td>" + rs.getString(5) + "</td>");
                        out.println("<td>" + rs.getString(6) + "</td>");
                        out.println("<td>" + rs.getString(7) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        
      out.write("\n");
      out.write("                     </tbody>\n");
      out.write("                 </table>\n");
      out.write("         </div>\n");
      out.write("         <div class=\"tablas\">\n");
      out.write("                 <h1> CURSOS </h1>\n");
      out.write("                 <table class=\"table table-hover\">\n");
      out.write("                     <thead>\n");
      out.write("                         <tr>\n");
      out.write("                             <th>ID</th>\n");
      out.write("                             <th>Nombre</th>\n");
      out.write("                             <th>Descripción</th>\n");
      out.write("                             <th>Duración</th>\n");
      out.write("                         </tr>\n");
      out.write("                     </thead>\n");
      out.write("                     <tbody>\n");
      out.write("                         ");

                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from cursos order by idCurso");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        
      out.write("\n");
      out.write("                     </tbody>\n");
      out.write("                 </table>\n");
      out.write("                 <center><button type=\"button\" class=\"btn btn-info btn-lg\" data-toggle=\"modal\" data-target=\"#myModal4\">Agregar</button></center>\n");
      out.write("         </div>     \n");
      out.write("         <div class=\"tablas\">\n");
      out.write("                 <h1> INSCRIPCIONES </h1>\n");
      out.write("                 <table class=\"table table-hover\">\n");
      out.write("                     <thead>\n");
      out.write("                         <tr>\n");
      out.write("                             <th>ID Cliente</th>\n");
      out.write("                             <th>ID Curso</th>\n");
      out.write("                             <th>Fecha de incripción</th>\n");
      out.write("                         </tr>\n");
      out.write("                     </thead>\n");
      out.write("                     <tbody>\n");
      out.write("                         ");

                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from inscripciones order by idCliente");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getDate(3) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        
      out.write("\n");
      out.write("                     </tbody>\n");
      out.write("                 </table>\n");
      out.write("         </div>     \n");
      out.write("         <div class=\"tablas\">\n");
      out.write("                 <h1> TARJETAS DE CREDITO </h1>\n");
      out.write("                 <table class=\"table table-hover\">\n");
      out.write("                     <thead>\n");
      out.write("                         <tr>\n");
      out.write("                             <th>ID Cliente</th>\n");
      out.write("                             <th>ID Tarjeta</th>\n");
      out.write("                             <th>Número de tarjeta</th>\n");
      out.write("                             <th>Código de seguridad</th>\n");
      out.write("                         </tr>\n");
      out.write("                     </thead>\n");
      out.write("                     <tbody>\n");
      out.write("                         ");

                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from tarjetas_credito order by idCliente");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        
      out.write("\n");
      out.write("                     </tbody>\n");
      out.write("                 </table>\n");
      out.write("         </div>   \n");
      out.write("         <div class=\"tablas\">\n");
      out.write("                 <h1> TRANSPORTISTAS </h1>\n");
      out.write("                 <table class=\"table table-hover\">\n");
      out.write("                     <thead>\n");
      out.write("                         <tr>\n");
      out.write("                             <th>ID</th>\n");
      out.write("                             <th>Nombre</th>\n");
      out.write("                         </tr>\n");
      out.write("                     </thead>\n");
      out.write("                     <tbody>\n");
      out.write("                         ");

                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from transportista order by idTransportista");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        
      out.write("\n");
      out.write("                     </tbody>\n");
      out.write("                 </table>\n");
      out.write("                 <center><button type=\"button\" class=\"btn btn-info btn-lg\" data-toggle=\"modal\" data-target=\"#myModal5\">Agregar</button></center>\n");
      out.write("         </div>\n");
      out.write("         <div class=\"tablas\">\n");
      out.write("                 <h1> PRODUCTOS </h1>\n");
      out.write("                 <table class=\"table table-hover\">\n");
      out.write("                     <thead>\n");
      out.write("                         <tr>\n");
      out.write("                             <th>ID</th>\n");
      out.write("                             <th>Existencia</th>\n");
      out.write("                             <th>Precio</th>\n");
      out.write("                         </tr>\n");
      out.write("                     </thead>\n");
      out.write("                     <tbody>\n");
      out.write("                         ");

                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from productos order by idProducto");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + "$ " + rs.getString(3) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        
      out.write("\n");
      out.write("                     </tbody>\n");
      out.write("                 </table>\n");
      out.write("         </div>\n");
      out.write("         <div class=\"tablas\">\n");
      out.write("                 <h1> INSTRUMENTOS </h1>\n");
      out.write("                 <table class=\"table table-hover\">\n");
      out.write("                     <thead>\n");
      out.write("                         <tr>\n");
      out.write("                             <th>ID Producto</th>\n");
      out.write("                             <th>ID Instrumento</th>\n");
      out.write("                             <th>Nombre</th>\n");
      out.write("                             <th>Descripción</th>\n");
      out.write("                         </tr>\n");
      out.write("                     </thead>\n");
      out.write("                     <tbody>\n");
      out.write("                         ");

                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from instrumentos order by idProducto");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        
      out.write("\n");
      out.write("                     </tbody>\n");
      out.write("                 </table>\n");
      out.write("                 <center><button type=\"button\" class=\"btn btn-info btn-lg\" data-toggle=\"modal\" data-target=\"#myModal\">Agregar</button></center>\n");
      out.write("                 <center><button type=\"button\" class=\"btn btn-info btn-lg\" data-toggle=\"modal\" data-target=\"#myModal\">Borrar</button></center>\n");
      out.write("         </div>\n");
      out.write("         <div class=\"tablas\">\n");
      out.write("                 <h1> ALBUMES </h1>\n");
      out.write("                 <table class=\"table table-hover\">\n");
      out.write("                     <thead>\n");
      out.write("                         <tr>\n");
      out.write("                             <th>ID Producto</th>\n");
      out.write("                             <th>ID Album</th>\n");
      out.write("                             <th>Nombre</th>\n");
      out.write("                             <th>Artista</th>\n");
      out.write("                             <th>Género</th>\n");
      out.write("                             <th>Año</th>\n");
      out.write("                         </tr>\n");
      out.write("                     </thead>\n");
      out.write("                     <tbody>\n");
      out.write("                         ");

                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from albumes order by idProducto");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("<td>" + rs.getString(5) + "</td>");
                        out.println("<td>" + rs.getString(6) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        
      out.write("\n");
      out.write("                     </tbody>\n");
      out.write("                 </table>\n");
      out.write("                 <center><button type=\"button\" class=\"btn btn-info btn-lg\" data-toggle=\"modal\" data-target=\"#myModal2\">Agregar</button></center>\n");
      out.write("         </div>\n");
      out.write("         <div class=\"tablas\">\n");
      out.write("                 <h1> ACCESORIOS </h1>\n");
      out.write("                 <table class=\"table table-hover\">\n");
      out.write("                     <thead>\n");
      out.write("                         <tr>\n");
      out.write("                             <th>ID Producto</th>\n");
      out.write("                             <th>ID Accesorio</th>\n");
      out.write("                             <th>Nombre</th>\n");
      out.write("                             <th>Descripción</th>\n");
      out.write("                         </tr>\n");
      out.write("                     </thead>\n");
      out.write("                     <tbody>\n");
      out.write("                         ");

                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from accesorios order by idProducto");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        
      out.write("\n");
      out.write("                     </tbody>\n");
      out.write("                 </table>\n");
      out.write("                 <center><button type=\"button\" class=\"btn btn-info btn-lg\" data-toggle=\"modal\" data-target=\"#myModal3\">Agregar</button></center>\n");
      out.write("         </div>\n");
      out.write("         <div class=\"tablas\">\n");
      out.write("                 <h1> COMPRAS </h1>\n");
      out.write("                 <table class=\"table table-hover\">\n");
      out.write("                     <thead>\n");
      out.write("                         <tr>\n");
      out.write("                             <th>ID Compra</th>\n");
      out.write("                             <th>ID Cliente</th>\n");
      out.write("                             <th>ID Producto</th>\n");
      out.write("                             <th>Cantidad</th>\n");
      out.write("                             <th>Subtotal</th>\n");
      out.write("                             <th>Iva</th>\n");
      out.write("                             <th>Total</th>\n");
      out.write("                             <th>Fecha de llegada</th>\n");
      out.write("                             <th>ID Tarjeta</th>\n");
      out.write("                             <th>ID Dirección</th>\n");
      out.write("                             <th>ID Transportista</th>\n");
      out.write("                         </tr>\n");
      out.write("                     </thead>\n");
      out.write("                     <tbody>\n");
      out.write("                         ");

                        try {
                        Connection cnx = DataBaseConnect.getConnection();
                        Statement st = cnx.createStatement();
                        ResultSet rs =  st.executeQuery("select * from compras order by idCompra");
                        while (rs.next()) {
                        out.println("<tr>");
                        out.println("<td>" + rs.getString(1) + "</td>");
                        out.println("<td>" + rs.getString(2) + "</td>");
                        out.println("<td>" + rs.getString(3) + "</td>");
                        out.println("<td>" + rs.getString(4) + "</td>");
                        out.println("<td>" + rs.getString(5) + "</td>");
                        out.println("<td>" + rs.getString(6) + "</td>");
                        out.println("<td>" + rs.getString(7) + "</td>");
                        out.println("<td>" + rs.getDate(8) + "</td>");
                        out.println("<td>" + rs.getString(9) + "</td>");
                        out.println("<td>" + rs.getString(10) + "</td>");
                        out.println("<td>" + rs.getString(11) + "</td>");
                        out.println("</tr>");
                        }
                        }catch(Exception e){
                        e.printStackTrace();
                     }
                        
      out.write("\n");
      out.write("                     </tbody>\n");
      out.write("                 </table>\n");
      out.write("         </div>\n");
      out.write("     </div>\n");
      out.write("     \n");
      out.write("      <!-- Modal -->\n");
      out.write("<div id=\"myModal\" class=\"modal fade\" role=\"dialog\">\n");
      out.write("  <div class=\"modal-dialog\">\n");
      out.write("\n");
      out.write("    <!-- Modal content-->\n");
      out.write("    <div class=\"modal-content\">\n");
      out.write("      <div class=\"modal-header\">\n");
      out.write("        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n");
      out.write("        <h4 class=\"modal-title\">Agregar instrumento</h4>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"modal-body\">\n");
      out.write("          <form action='instrumentoagr' method='post'>\n");
      out.write("            <label>ID Producto</label><input type='text' class='form-control'  name='idprod' placeholder='Ingresa el ID del producto'><br/>\n");
      out.write("            <label>ID Instrumento</label><input type='text' class='form-control' name='idins' placeholder='Ingresa el ID del instrumento'><br/>\n");
      out.write("            <label>Existencia</label><input type='text' class='form-control'  name='exist' placeholder='Ingresa la existencia del producto'><br/>\n");
      out.write("            <label>Precio</label><input type='text' class='form-control'  name='prec' placeholder='Ingresa el precio del producto'><br/>\n");
      out.write("            <label>Nombre</label><input type='text' class='form-control'  name='nomins' placeholder='Ingresa el nombre del instrumento'><br/>\n");
      out.write("            <label>Descripción</label><input type='text' class='form-control' name='descins' placeholder='Ingresa la descripción del instrumento '><br/>\n");
      out.write("            <input type='submit' value='Agregar instrumento' class='btn btn-success btn-block'/>\n");
      out.write("          </form>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"modal-footer\">\n");
      out.write("        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cerrar</button>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("  </div>\n");
      out.write("</div>\n");
      out.write("      \n");
      out.write(" <!-- Modal -->\n");
      out.write("<div id=\"myModal2\" class=\"modal fade\" role=\"dialog\">\n");
      out.write("  <div class=\"modal-dialog\">\n");
      out.write("\n");
      out.write("    <!-- Modal content-->\n");
      out.write("    <div class=\"modal-content\">\n");
      out.write("      <div class=\"modal-header\">\n");
      out.write("        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n");
      out.write("        <h4 class=\"modal-title\">Agregar album</h4>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"modal-body\">\n");
      out.write("          <form action='albumagr' method='post'>\n");
      out.write("            <label>ID Producto</label><input type='text' class='form-control'  name='idprod' placeholder='Ingresa el ID del producto'><br/>\n");
      out.write("            <label>ID Album</label><input type='text' class='form-control' name='idalb' placeholder='Ingresa el ID del album'><br/>\n");
      out.write("            <label>Existencia</label><input type='text' class='form-control'  name='exist' placeholder='Ingresa la existencia del producto'><br/>\n");
      out.write("            <label>Precio</label><input type='text' class='form-control'  name='prec' placeholder='Ingresa el precio del producto'><br/>\n");
      out.write("            <label>Nombre</label><input type='text' class='form-control'  name='nomalb' placeholder='Ingresa el nombre del album'><br/>\n");
      out.write("            <label>Artista</label><input type='text' class='form-control' name='artis' placeholder='Ingresa el artista o grupo'><br/>\n");
      out.write("            <label>Género</label><input type='text' class='form-control' name='gene' placeholder='Ingresa el género'><br/>\n");
      out.write("            <label>Año</label><input type='text' class='form-control' name='anio' placeholder='Ingresa el año de publicación'><br/>\n");
      out.write("            <input type='submit' value='Agregar album' class='btn btn-success btn-block'/>\n");
      out.write("          </form>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"modal-footer\">\n");
      out.write("        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cerrar</button>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("  </div>\n");
      out.write("</div>\n");
      out.write(" \n");
      out.write(" <!-- Modal -->\n");
      out.write("<div id=\"myModal3\" class=\"modal fade\" role=\"dialog\">\n");
      out.write("  <div class=\"modal-dialog\">\n");
      out.write("\n");
      out.write("    <!-- Modal content-->\n");
      out.write("    <div class=\"modal-content\">\n");
      out.write("      <div class=\"modal-header\">\n");
      out.write("        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n");
      out.write("        <h4 class=\"modal-title\">Agregar accesorio</h4>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"modal-body\">\n");
      out.write("          <form action='accesorioagr' method='post'>\n");
      out.write("            <label>ID Producto</label><input type='text' class='form-control'  name='idprod' placeholder='Ingresa el ID del producto'><br/>\n");
      out.write("            <label>ID Accesorio</label><input type='text' class='form-control' name='idacc' placeholder='Ingresa el ID del accesorio'><br/>\n");
      out.write("            <label>Existencia</label><input type='text' class='form-control'  name='exist' placeholder='Ingresa la existencia del producto'><br/>\n");
      out.write("            <label>Precio</label><input type='text' class='form-control'  name='prec' placeholder='Ingresa el precio del producto'><br/>\n");
      out.write("            <label>Nombre</label><input type='text' class='form-control'  name='nomacc' placeholder='Ingresa el nombre del accesorio'><br/>\n");
      out.write("            <label>Descripción</label><input type='text' class='form-control' name='descacc' placeholder='Ingresa la descripción del accesorio'><br/>\n");
      out.write("            <input type='submit' value='Agregar accesorio' class='btn btn-success btn-block'/>\n");
      out.write("          </form>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"modal-footer\">\n");
      out.write("        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cerrar</button>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("  </div>\n");
      out.write("</div>\n");
      out.write(" \n");
      out.write(" <!-- Modal -->\n");
      out.write("<div id=\"myModal4\" class=\"modal fade\" role=\"dialog\">\n");
      out.write("  <div class=\"modal-dialog\">\n");
      out.write("\n");
      out.write("    <!-- Modal content-->\n");
      out.write("    <div class=\"modal-content\">\n");
      out.write("      <div class=\"modal-header\">\n");
      out.write("        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n");
      out.write("        <h4 class=\"modal-title\">Agregar curso</h4>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"modal-body\">\n");
      out.write("          <form action='cursosagr' method='post'>\n");
      out.write("            <label>ID Curso</label><input type='text' class='form-control'  name='idcur' placeholder='Ingresa el ID del curso'><br/>\n");
      out.write("            <label>Nombre</label><input type='text' class='form-control'  name='nomcur' placeholder='Ingresa el nombre del curso'><br/>\n");
      out.write("            <label>Descripción</label><input type='text' class='form-control' name='desccur' placeholder='Ingresa la descripción del curso'><br/>\n");
      out.write("            <label>Duración</label><input type='text' class='form-control' name='dura' placeholder='Ingresa la duración del curso'><br/>\n");
      out.write("            <input type='submit' value='Agregar curso' class='btn btn-success btn-block'/>\n");
      out.write("          </form>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"modal-footer\">\n");
      out.write("        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cerrar</button>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("  </div>\n");
      out.write("</div>  \n");
      out.write(" \n");
      out.write("<!-- Modal -->\n");
      out.write("<div id=\"myModal5\" class=\"modal fade\" role=\"dialog\">\n");
      out.write("  <div class=\"modal-dialog\">\n");
      out.write("\n");
      out.write("    <!-- Modal content-->\n");
      out.write("    <div class=\"modal-content\">\n");
      out.write("      <div class=\"modal-header\">\n");
      out.write("        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n");
      out.write("        <h4 class=\"modal-title\">Agregar transportista</h4>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"modal-body\">\n");
      out.write("          <form action='transportistaagr' method='post'>\n");
      out.write("            <label>ID Transportista</label><input type='text' class='form-control'  name='idtrans' placeholder='Ingresa el ID del transportista'><br/>\n");
      out.write("            <label>Nombre</label><input type='text' class='form-control'  name='nomtrans' placeholder='Ingresa el nombre del transportista'><br/>\n");
      out.write("            <input type='submit' value='Agregar transportista' class='btn btn-success btn-block'/>\n");
      out.write("          </form>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"modal-footer\">\n");
      out.write("        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cerrar</button>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("  </div>\n");
      out.write("</div>\n");
      out.write(" \n");
      out.write("    <!-- Bootsrap necesita jQuery para funcionar -->\n");
      out.write("      <script type=\"text/javascript\" src=\"js/jquery-3.2.0.min.js\"></script>\n");
      out.write("      <!-- Llamamos al JavaScript de Bootstrap-->\n");
      out.write("      <script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("                   \n");
      out.write("     </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
