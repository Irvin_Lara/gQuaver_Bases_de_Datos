package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class cursos_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("      <title>Cursos gQuaver</title>\n");
      out.write("      <meta charset=\"utf-8\">\n");
      out.write("      <!-- se vea bien en dispositivos móviles-->\n");
      out.write("      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("      <!--Llamamos al archivo CSS -->\n");
      out.write("      <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\">\n");
      out.write("      <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"icons/icon.png\" />\n");
      out.write("      <link rel=\"stylesheet\" href=\"css/style.css\">\n");
      out.write("      <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("          <div id=\"header\">\n");
      out.write("         <nav class=\"navbar navbar-inverse navbar-fixed-top\">\n");
      out.write("            <img class=\"icon\" src=\"icons/icon.png\" alt=\"icono guitarra\">\n");
      out.write("            <div class=\"container-fluid\">\n");
      out.write("               <div class=\"navbar-header\">\n");
      out.write("                  <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#myNavbar\">\n");
      out.write("                  <span class=\"icon-bar\"></span>\n");
      out.write("                  </button>\n");
      out.write("                  <a id=\"gQuaver\" class=\"navbar-brand\" href=\"#\">gQuaver</a>\n");
      out.write("               </div>\n");
      out.write("               <div>\n");
      out.write("                  <div class=\"collapse navbar-collapse\" id=\"myNavbar\">\n");
      out.write("                     <ul class=\"nav navbar-nav\">\n");
      out.write("                        <li><a class=\"subir\" href=\"#section1\">Cursos</a></li>\n");
      out.write("                        <li><button type=\"button\" class=\"btn btn-default navbar-btn\" id=\"myBtn\"> ¡Inicia sesión o regístrate!</button></li>\n");
      out.write("                     </ul>\n");
      out.write("                  </div>\n");
      out.write("               </div>\n");
      out.write("            </div>\n");
      out.write("         </nav>\n");
      out.write("      </div>\n");
      out.write("        <div id=\"section3\">\n");
      out.write("            <div class=\"container\">\n");
      out.write("\t<div class=\"row\">\n");
      out.write("\t\t<h2 class=\"text-center\">Cursos</h2>\n");
      out.write("                <p>gQuaver te ofrece los mejores cursos en línea para que aprendas a desenvolverte como todo un profesional.</p>\n");
      out.write("        <hr/>\n");
      out.write("\t</div>\n");
      out.write("    <div class=\"row\">\n");
      out.write("        <div class=\"col-sm-6 col-md-4\">\n");
      out.write("            <div class=\"thumbnail\">\n");
      out.write("                <h4>\n");
      out.write("                    <span class=\"label label-info info\">\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"viewed\">257 <b class=\"glyphicon glyphicon-eye-open\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"viewed\">3 <b class=\"glyphicon glyphicon-star\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Bootstrap version\">3.0.3</span>\n");
      out.write("                    </span>\n");
      out.write("                </h4>\n");
      out.write("                <img src=\"images/armoniaC.jpg\" alt=\"armonía musical\">\n");
      out.write("                <a href=\"\" class=\"btn btn-primary col-xs-12\" role=\"button\">Armonía musical</a>\n");
      out.write("                <div class=\"clearfix\"></div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        \n");
      out.write("        <div class=\"col-sm-6 col-md-4\">\n");
      out.write("            <div class=\"thumbnail\">\n");
      out.write("                <h4>\n");
      out.write("                    <img src=\"images/pianoC.jpg\" alt=\"piano\">\n");
      out.write("                    <span class=\"label label-info info\">\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Viewed\">433 <i class=\"glyphicon glyphicon-eye-open\"></i></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Favorited\">4 <i class=\"glyphicon glyphicon-star\"></i></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Bootstrap version\">3.0.3</span>\n");
      out.write("                    </span>\n");
      out.write("                </h4>\n");
      out.write("                <a href=\"\" class=\"btn btn-primary col-xs-12\" role=\"button\">Piano y teclado</a>\n");
      out.write("                <div class=\"clearfix\"></div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        \n");
      out.write("        <div class=\"col-sm-6 col-md-4\">\n");
      out.write("            <div class=\"thumbnail\">\n");
      out.write("                <h4>\n");
      out.write("                    <img src=\"images/acusticaC.jpg\" alt=\"guitara acústica\">\n");
      out.write("                    <span class=\"label label-info info\">\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Viewed\">2.1K <b class=\"glyphicon glyphicon-eye-open\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Favorited\">13 <b class=\"glyphicon glyphicon-star\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Bootstrap version\">3.0.3</span>\n");
      out.write("                    </span>\n");
      out.write("                </h4>\n");
      out.write("                <a href=\"\" class=\"btn btn-primary col-xs-12\" role=\"button\">Guitarra acústica</a>\n");
      out.write("                <div class=\"clearfix\"></div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        \n");
      out.write("        <div class=\"col-sm-6 col-md-4\">\n");
      out.write("            <div class=\"thumbnail\">\n");
      out.write("                <h4>\n");
      out.write("                    <img src=\"images/electricaC.jpg\" alt=\"guitarra eléctrica\">\n");
      out.write("                    <span class=\"label label-info info\">\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Viewed\">11.5K <b class=\"glyphicon glyphicon-eye-open\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Favorited\">24 <b class=\"glyphicon glyphicon-star\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Bootstrap version\">3.0.3</span>\n");
      out.write("                    </span>\n");
      out.write("                </h4>\n");
      out.write("                <a href=\"\" class=\"btn btn-primary col-xs-12\" role=\"button\">Guitarra eléctrica</a>\n");
      out.write("                <div class=\"clearfix\"></div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        \n");
      out.write("        <div class=\"col-sm-6 col-md-4\">\n");
      out.write("            <div class=\"thumbnail\">\n");
      out.write("                <h4>\n");
      out.write("                    <img src=\"images/bajoC.jpg\" alt=\"bajo eléctrico\">\n");
      out.write("                    <span class=\"label label-info info\">\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Viewed\">7.3K <b class=\"glyphicon glyphicon-eye-open\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Favorited\">31 <b class=\"glyphicon glyphicon-star\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Bootstrap version\">3.0.3</span>\n");
      out.write("                    </span>\n");
      out.write("                </h4>\n");
      out.write("                <a href=\"\" class=\"btn btn-primary col-xs-12\" role=\"button\">Bajo eléctrico</a>\n");
      out.write("                <div class=\"clearfix\"></div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        \n");
      out.write("        <div class=\"col-sm-6 col-md-4\">\n");
      out.write("            <div class=\"thumbnail\">\n");
      out.write("                <h4>\n");
      out.write("                    <img src=\"images/saxofonC.jpg\" alt=\"saxofón\">\n");
      out.write("                    <span class=\"label label-info info\">\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Viewed\">2.5K <b class=\"glyphicon glyphicon-eye-open\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Favorited\">18 <b class=\"glyphicon glyphicon-star\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Bootstrap version\">3.0.0</span>\n");
      out.write("                    </span>\n");
      out.write("                </h4>\n");
      out.write("                <a href=\"\" class=\"btn btn-primary col-xs-12\" role=\"button\">Saxofón</a>\n");
      out.write("                <div class=\"clearfix\"></div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"col-sm-6 col-md-4\">\n");
      out.write("            <div class=\"thumbnail\">\n");
      out.write("                <h4>\n");
      out.write("                    <img src=\"images/bateriaC.jpg\" alt=\"bateria\">\n");
      out.write("                    <span class=\"label label-info info\">\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Viewed\">1.5K <b class=\"glyphicon glyphicon-eye-open\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Favorited\">20 <b class=\"glyphicon glyphicon-star\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Bootstrap version\">3.0.0</span>\n");
      out.write("                    </span>\n");
      out.write("                </h4>\n");
      out.write("                <a href=\"\" class=\"btn btn-primary col-xs-12\" role=\"button\">Batería</a>\n");
      out.write("                <div class=\"clearfix\"></div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"col-sm-6 col-md-4\">\n");
      out.write("            <div class=\"thumbnail\">\n");
      out.write("                <h4>\n");
      out.write("                    <img src=\"images/violinC.jpg\" alt=\"violin\">\n");
      out.write("                    <span class=\"label label-info info\">\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Viewed\">5.5K <b class=\"glyphicon glyphicon-eye-open\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Favorited\">25 <b class=\"glyphicon glyphicon-star\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Bootstrap version\">3.0.0</span>\n");
      out.write("                    </span>\n");
      out.write("                </h4>\n");
      out.write("                <a href=\"\" class=\"btn btn-primary col-xs-12\" role=\"button\">Violín</a>\n");
      out.write("                <div class=\"clearfix\"></div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"col-sm-6 col-md-4\">\n");
      out.write("            <div class=\"thumbnail\">\n");
      out.write("                <h4>\n");
      out.write("                    <img src=\"images/cantoC.jpg\" alt=\"canto\">\n");
      out.write("                    <span class=\"label label-info info\">\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Viewed\">2.0K <b class=\"glyphicon glyphicon-eye-open\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Favorited\">14 <b class=\"glyphicon glyphicon-star\"></b></span>\n");
      out.write("                        <span data-toggle=\"tooltip\" title=\"Bootstrap version\">3.0.0</span>\n");
      out.write("                    </span>\n");
      out.write("                </h4>\n");
      out.write("                <a href=\"\" class=\"btn btn-primary col-xs-12\" role=\"button\">Canto</a>\n");
      out.write("                <div class=\"clearfix\"></div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</div>\n");
      out.write("        </div>\n");
      out.write("      \n");
      out.write("       <!-- Bootsrap necesita jQuery para funcionar -->\n");
      out.write("      <script type=\"text/javascript\" src=\"js/jquery-3.2.0.min.js\"></script>\n");
      out.write("      <script type=\"text/javascript\" src=\"js/script.js\"></script>\n");
      out.write("      <!-- Llamamos al JavaScript de Bootstrap-->\n");
      out.write("      <script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
