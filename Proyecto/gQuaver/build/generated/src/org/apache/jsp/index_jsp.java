package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import ConexionSQLDB.DataBaseConnect;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write(" \n");
      out.write(" \n");
      out.write(" \n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("   <head>\n");
      out.write("      <title>gQuaver</title>\n");
      out.write("      <meta charset=\"utf-8\">\n");
      out.write("      <!-- se vea bien en dispositivos móviles-->\n");
      out.write("      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("      <!--Llamamos al archivo CSS -->\n");
      out.write("      <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\">\n");
      out.write("      <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"icons/icon.png\" />\n");
      out.write("      <link rel=\"stylesheet\" href=\"css/style.css\">\n");
      out.write("      <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">\n");
      out.write("   </head>\n");
      out.write("   <body>\n");
      out.write("     <div class=\"todo\">\n");
      out.write("      <div id=\"header\">\n");
      out.write("         <nav class=\"navbar navbar-inverse navbar-fixed-top\">\n");
      out.write("            <img class=\"icon\" src=\"icons/icon.png\" alt=\"icono guitarra\">\n");
      out.write("            <div class=\"container-fluid\">\n");
      out.write("               <div class=\"navbar-header\">\n");
      out.write("                  <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#myNavbar\">\n");
      out.write("                  <span class=\"icon-bar\"></span>\n");
      out.write("                  <span class=\"icon-bar\"></span>\n");
      out.write("                  <span class=\"icon-bar\"></span>\n");
      out.write("                  </button>\n");
      out.write("                  <a id=\"gQuaver\" class=\"navbar-brand\" href=\"#\">gQuaver</a>\n");
      out.write("               </div>\n");
      out.write("               <div>\n");
      out.write("                  <div class=\"collapse navbar-collapse\" id=\"myNavbar\">\n");
      out.write("                     <ul class=\"nav navbar-nav\">\n");
      out.write("                        <li><a class=\"subir\" href=\"#section1\">Subir página</a></li>\n");
      out.write("                        <li><a class=\"enlace\" href=\"#section2\">Nosotros</a></li>\n");
      out.write("                        <li><a class=\"enlace\" href=\"#section4\">Contáctanos</a></li>\n");
      out.write("                        <a href=\"login.jsp\"><li><button type=\"button\" class=\"btn btn-default navbar-btn\" id=\"myBtn\"> ¡Inicia sesión o regístrate!</button></li></a>\n");
      out.write("                     </ul>\n");
      out.write("                  </div>\n");
      out.write("               </div>\n");
      out.write("            </div>\n");
      out.write("         </nav>\n");
      out.write("      </div>\n");
      out.write("      <div id=\"main\">\n");
      out.write("         <div id=\"section1\" class=\"container-fluid\">\n");
      out.write("            <div id=\"myCarousel\" class=\"carousel slide\" data-ride=\"carousel\">\n");
      out.write("               <!-- Indicators -->\n");
      out.write("               <ol class=\"carousel-indicators\">\n");
      out.write("                  <li data-target=\"#myCarousel\" data-slide-to=\"0\" class=\"active\"></li>\n");
      out.write("                  <li data-target=\"#myCarousel\" data-slide-to=\"1\"></li>\n");
      out.write("                  <li data-target=\"#myCarousel\" data-slide-to=\"2\"></li>\n");
      out.write("                  <li data-target=\"#myCarousel\" data-slide-to=\"3\"></li>\n");
      out.write("                  <li data-target=\"#myCarousel\" data-slide-to=\"4\"></li>\n");
      out.write("               </ol>\n");
      out.write("               <!-- Wrapper for slides -->\n");
      out.write("               <div class=\"carousel-inner\" role=\"listbox\">\n");
      out.write("                  <div class=\"item active\">\n");
      out.write("                     <img src=\"images/piano.jpg\" alt=\"piano\">\n");
      out.write("                     <div class=\"carousel-caption\">\n");
      out.write("                        <h3>Tu brillas, nosotros te ayudamos a mejorar</h3>\n");
      out.write("                        <p>Te ofrecemos los mejores instrumentos musicales.</p>\n");
      out.write("                     </div>\n");
      out.write("                  </div>\n");
      out.write("                  <div class=\"item\">\n");
      out.write("                     <img src=\"images/guitarra.jpg\" alt=\"guitarra\">\n");
      out.write("                     <div class=\"carousel-caption\">\n");
      out.write("                        <h3>Tan únicos como tú</h3>\n");
      out.write("                        <p>Consigue los mejores accesorios y adornos para tus intrumentos musicales.</p>\n");
      out.write("                     </div>\n");
      out.write("                  </div>\n");
      out.write("                  <div class=\"item\">\n");
      out.write("                     <img src=\"images/bateria.jpg\" alt=\"bateria\">\n");
      out.write("                     <div class=\"carousel-caption\">\n");
      out.write("                        <h3>Que nada te impida llegar lejos</h3>\n");
      out.write("                        <p>¿Se descompuso tu amplificador? ¿Se rompió alguna baqueta? Aquí puedes conseguir lo que te haga falta.</p>\n");
      out.write("                     </div>\n");
      out.write("                  </div>\n");
      out.write("                  <div class=\"item\">\n");
      out.write("                     <img src=\"images/orquesta.jpg\" alt=\"orquesta\">\n");
      out.write("                     <div class=\"carousel-caption\">\n");
      out.write("                        <h3>Para profesionales y principantes</h3>\n");
      out.write("                        <p>Además, te ofrecemos los mejores cursos, que se adaptan a tu nivel de aprendizaje.</p>\n");
      out.write("                     </div>\n");
      out.write("                  </div>\n");
      out.write("                  <div class=\"item\">\n");
      out.write("                     <img src=\"images/concierto.jpg\" alt=\"concierto\">\n");
      out.write("                     <div class=\"carousel-caption\">\n");
      out.write("                        <h3>Como las estrellas</h3>\n");
      out.write("                        <p>Que nada te detenga, aquí conseguirás los productos de la más alta calidad que te convertirán en toda una estrella de la música.</p>\n");
      out.write("                     </div>\n");
      out.write("                  </div>\n");
      out.write("               </div>\n");
      out.write("               <a class=\"left carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"prev\">\n");
      out.write("               <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>\n");
      out.write("               <span class=\"sr-only\">Anterior</span>\n");
      out.write("               </a>\n");
      out.write("               <a class=\"right carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"next\">\n");
      out.write("               <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>\n");
      out.write("               <span class=\"sr-only\">Siguiente</span>\n");
      out.write("               </a>\n");
      out.write("            </div>\n");
      out.write("         </div>\n");
      out.write("         <div class=\"contSection\">\n");
      out.write("            <div class=\"parallax\">\n");
      out.write("               <div id=\"section2\" class=\"container-fluid\">\n");
      out.write("                  <h1>Calidad, excelencia y profesionalismo</h1>\n");
      out.write("               </div>\n");
      out.write("            </div>\n");
      out.write("            <div id=\"section3\" class=\"container-fluid\">\n");
      out.write("               <img class=\"icon2\" src=\"icons/icon.png\" alt=\"icono guitarra\">\n");
      out.write("               <h1>gQuaver</h1>\n");
      out.write("               <br/>\n");
      out.write("               <iframe class=\"ytComercial\" src=\"https://www.youtube.com/embed/FnNOAwo4krw\" frameborder=\"0\" allowfullscreen></iframe>\n");
      out.write("               <h2>¿Quiénes somos?</h2>\n");
      out.write("               <br/>\n");
      out.write("               <p>gQuaver, S.A. inicia su fructífera labor en los albores de 1971.\n");
      out.write("                  Funcionando en principio con distribución local y regional en la comercialización de\n");
      out.write("                  aparatos y refacciones electrónicas. Amplia su cobertura en la década de los 80s a nivel\n");
      out.write("                  nacional estableciendo filiales en México, Torreón, Guadalajara, Monterrey y Querétaro.\n");
      out.write("               </p>\n");
      out.write("               <p>Nuestra misión es ser los mejores en servicio, atención y precio, contando con un extenso\n");
      out.write("                  surtido en el ramo de la electrónica y la música, siempre trabajando en equipo y con\n");
      out.write("                  honestidad para ganarnos el respeto y la lealtad de nuestros clientes.\n");
      out.write("               </p>\n");
      out.write("               <br/>\n");
      out.write("               <p>Pero no sólo vendemos intrumentos musicales, sino también te ofrecemos cursos para que\n");
      out.write("                  aprendas a ser todo un profesional.\n");
      out.write("               </p>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"parallaxInstruments\">\n");
      out.write("               <div id=\"section4\" class=\"container-fluid\">\n");
      out.write("                  <h1>Contáctanos</h1>\n");
      out.write("               </div>\n");
      out.write("            </div>\n");
      out.write("            <div id=\"section5\" class=\"container-fluid\">\n");
      out.write("               <img src=\"icons/ubicationIcon.png\" alt=\"icono ubicación\">\n");
      out.write("               <iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14003.763052702283!2d-106.1221763!3d28.6614923!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf0fb71e2e39d2851!2sJD+Electronics!5e0!3m2!1ses!2smx!4v1495334818895\"\n");
      out.write("               frameborder=\"0\"  allowfullscreen></iframe>\n");
      out.write("               <h2>Dirección</h2>\n");
      out.write("               <p>Plaza Arboledas, Av. Francisco Villa 4907, Int.208, Arboledas, 31110 Chihuahua, Chih.</p>\n");
      out.write("               <br/>\n");
      out.write("               <img src=\"icons/phoneIcon.png\" alt=\"icono teléfono\">\n");
      out.write("               <h2>Teléfono</h2>\n");
      out.write("               <p> 01 614 417 2888</p>\n");
      out.write("               <br/>\n");
      out.write("               <img src=\"icons/mailIcon.png\" alt=\"icono mail\">\n");
      out.write("               <h2>Correo electrónico</h2>\n");
      out.write("               <p>info@jdelectronics.com.mx</p>\n");
      out.write("               <br/>\n");
      out.write("               <img src=\"icons/watchIcon.png\" alt=\"icono reloj\">\n");
      out.write("               <h2>Horario de atención</h2>\n");
      out.write("               <p>Lunes a sábado de 9:00hrs. a 19:30hrs.</p>\n");
      out.write("               <br/>\n");
      out.write("               <br/>\n");
      out.write("            </div>\n");
      out.write("         </div>\n");
      out.write("      </div>\n");
      out.write("      <div id=\"footer\">\n");
      out.write("      <div class=\"container-fluid bg-primary py-3\">\n");
      out.write("         <div class=\"container\">\n");
      out.write("            <div class=\"row py-3\">\n");
      out.write("               <div class=\"col-md-9\">\n");
      out.write("                  <p class=\"text-white\">Copyright (c) gQuaver 2017 Todos los derechos reservados.</p>\n");
      out.write("               </div>\n");
      out.write("               <div class=\"col-md-3\">\n");
      out.write("                  <div class=\"d-inline-block\">\n");
      out.write("                     <div class=\"bg-circle-outline d-inline-block\">\n");
      out.write("                        <a href=\"https://www.facebook.com/\" class=\"text-white\"><i class=\"fa fa-2x fa-fw fa-facebook\"></i></a>\n");
      out.write("                     </div>\n");
      out.write("                     <div class=\"bg-circle-outline d-inline-block\">\n");
      out.write("                        <a href=\"https://twitter.com/\" class=\"text-white\">\n");
      out.write("                        <i class=\"fa fa-2x fa-fw fa-twitter\"></i></a>\n");
      out.write("                     </div>\n");
      out.write("                     <div class=\"bg-circle-outline d-inline-block\">\n");
      out.write("                        <a href=\"https://www.linkedin.com/company/\" class=\"text-white\">\n");
      out.write("                        <i class=\"fa fa-2x fa-fw fa-linkedin\"></i></a>\n");
      out.write("                     </div>\n");
      out.write("                  </div>\n");
      out.write("               </div>\n");
      out.write("            </div>\n");
      out.write("         </div>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("      <!-- Bootsrap necesita jQuery para funcionar -->\n");
      out.write("      <script type=\"text/javascript\" src=\"js/jquery-3.2.0.min.js\"></script>\n");
      out.write("      <script type=\"text/javascript\" src=\"js/script.js\"></script>\n");
      out.write("      <!-- Llamamos al JavaScript de Bootstrap-->\n");
      out.write("      <script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("   </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
