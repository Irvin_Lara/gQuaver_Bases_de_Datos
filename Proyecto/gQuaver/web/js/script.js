
$(document).ready(function(){
  // 2048 * 998
  $(".todo").hide();
  $("body").css("background-color", "#333333");
  $("<div class='carga'><img src='icons/cargandoIcon.gif'></div>").insertBefore(".todo");
  $(window).on( "load", function(){
    $(".carga").hide(".carga");
    $("body").css("background-color", "#040404");
    $(".todo").show();
    $("<div class='ventana'><button class='cerrar'>Cerrar</button><img src='icons/icon.png'><h2>Cursos de teoría musical gratis</h2><a href='cursos.jsp'>Más información</a></div>").insertBefore(".todo");
    $(".ventana").show();
    $(".ventana").hide();
    $(".cerrar").click(function(){
    $(".ventana").remove();
  });
  setTimeout(function(){
  $(".ventana").fadeIn(2500);
  });
  });

  //------------------------------------------------
  $(".carousel-inner img").css("height", "628px");
  $(".contSection").hide();
    $("#myBtn").click(function(){
        $("#myModal").modal();
    });
  $(".enlace").click(function(){
    $(".contSection").show();
  });
  $("#gQuaver").click(function(){
    $("body").css("background-color", "#040404");
    $(".contSection").hide();
  });
  
  //-----------------------------------------------
  $('[data-toggle="tooltip"]').tooltip();
  
  //-----------------------------------------------
  //$("#reg2").hide();
  $("#reg").click(function(){
      $(".modal-footer").hide();
      $(".form-group").remove();
      $(".hola").remove();
      $("<form action='registro' method='post'>" +
         "<label for='usrname'><span class='glyphicon glyphicon-user'></span> Usuario</label>" +
         "<input type='text' class='form-control' id='usrname' name='usuario' placeholder='Ingresa tu usuario'></br>"+
         "<label for='psw'><span class='glyphicon glyphicon-eye-open'></span> Contraseña</label>" +
         "<input type='password' class='form-control' id='psw' name='contra'  placeholder='Ingresa contaseña'></br>" +
         "<b><label>Email</label></b><input type='text' class='form-control' id='email' name='email' placeholder='Ingresa email'><br/>" +
         "<label>Nombre</label><input type='text' class='form-control' id='nombre' name='nombre' placeholder='Ingresa nombre'><br/>" +
         "<label>Calle</label><input type='text' class='form-control' id='calle' name='calle' placeholder='Ingresa calle'><br/>" +
         "<label>Colonia</label><input type='text' class='form-control' id='colonia' name='colonia' placeholder='Ingresa colonia'><br/>" +
         "<label>Número de casa</label><input type='text' class='form-control' id='noCasa' name='numero' placeholder='Ingresa no. casa'><br/>" +
         "<label>Estado</label><input type='text' class='form-control' id='estado' name='estado' placeholder='Ingresa estado'><br/>" +
         "<label>Municipio</label><input type='text' class='form-control' id='municipio' name='municipio' placeholder='Ingresa municipio'><br/>"+
         "<label>Tarjeta</label><input type='text' class='form-control' id='tarjeta' name='tarjeta' placeholder='Ingresa número de tarjeta'><br/>" +
         "<label>Clave de seguridad</label><input type='text' class='form-control' id='clave' name='clave' placeholder='Ingresa la clave de seguridad de la tarjeta (3 digitos)'><br/>" +
         "<p class='ultimo'>Envía tus datos presionando el siguiente botón</p></form>").insertBefore(".modal-footer");
     // $("#iniNaranja").replaceWith("<input type='submit value='Registrarse' id='reg2' class='btn btn-success btn-block' />");
      $("#iniNaranja").remove();
      $(".ultimo").append("<input type='submit' value='Registrarse' id='reg2' class='btn btn-success btn-block' />");
  });
  
  $("#reg2").click(function(){
    $(".modal-footer").show();
    $(this).hide();
  });
  
  
});