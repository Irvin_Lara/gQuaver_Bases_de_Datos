<%-- 
    Document   : cursos
    Created on : 27-may-2017, 17:12:02
    Author     : Tiiko Papas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>Cursos gQuaver</title>
      <meta charset="utf-8">
      <!-- se vea bien en dispositivos móviles-->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!--Llamamos al archivo CSS -->
      <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
      <link rel="shortcut icon" type="image/x-icon" href="icons/icon.png" />
      <link rel="stylesheet" href="css/style.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div id="header">
         <nav class="navbar navbar-inverse navbar-fixed-top">
            <img class="icon" src="icons/icon.png" alt="icono guitarra">
            <div class="container-fluid">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  </button>
                  <a id="gQuaver" class="navbar-brand" href="#">gQuaver</a>
               </div>
               <div>
                  <div class="collapse navbar-collapse" id="myNavbar">
                     <ul class="nav navbar-nav">
                        <li><a class="subir" href="#section1">Cursos</a></li>
                        <a href="login.jsp"><li><button type="button" class="btn btn-default navbar-btn" id="myBtn"> ¡Inicia sesión o regístrate!</button></li></a>
                     </ul>
                  </div>
               </div>
            </div>
         </nav>
      </div>
        <div id="main">
          <div class="parallaxC">
                 <div id="section4" class="container-fluid">
                   <b><h1>Cursos</h1></b>
               </div>
          </div>
            <div class="cursos">
                <br/>
                <p>Regístrate o inicia sesión para tener acceso a los mejores cursos en línea
                para aprender, con profesores que son profesionales de la música.</p>
                <p>Adquiere una versión del curso gratis durante quince días, para ello, regístrate
                o inicia sesión.</p>
                <br/>
                <div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/armoniaC.jpg" alt="armonía musical">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="viewed">257 <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="viewed">3 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.3</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Armonía musical</p>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/pianoC.jpg" alt="piano">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">433 <i class="glyphicon glyphicon-eye-open"></i></span>
                        <span data-toggle="tooltip" title="Favorited">4 <i class="glyphicon glyphicon-star"></i></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.3</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Piano y teclado</p>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/acusticaC.jpg" alt="guitara acústica">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">2.1K <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="Favorited">13 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.3</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Guitarra acústica</p>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/eletricaC.jpg" alt="guitarra eléctrica">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">11.5K <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="Favorited">24 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.3</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Guitarra eléctrica</p>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/bajoC.jpg" alt="bajo eléctrico">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">7.3K <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="Favorited">31 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.3</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Bajo eléctrico</p>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/saxofonC.jpg" alt="saxofón">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">2.5K <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="Favorited">18 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.0</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Saxofón</p>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/bateriaC.jpg" alt="bateria">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">1.5K <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="Favorited">20 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.0</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Batería</p>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/violinC.jpg" alt="violin">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">5.5K <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="Favorited">25 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.0</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Violín</p>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <h4>
                    <img src="images/cantoC.jpg" alt="canto">
                    <span class="label label-info info">
                        <span data-toggle="tooltip" title="Viewed">2.0K <b class="glyphicon glyphicon-eye-open"></b></span>
                        <span data-toggle="tooltip" title="Favorited">14 <b class="glyphicon glyphicon-star"></b></span>
                        <span data-toggle="tooltip" title="Bootstrap version">3.0.0</span>
                    </span>
                </h4>
                <p class="btn btn-primary col-xs-12" role="button">Canto</p>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
            </div>
        </div>
      <div id="footer">
      <div class="container-fluid bg-primary py-3">
         <div class="container">
            <div class="row py-3">
               <div class="col-md-9">
                  <p class="text-white">Copyright (c) gQuaver 2017 Todos los derechos reservados.</p>
               </div>
               <div class="col-md-3">
                  <div class="d-inline-block">
                     <div class="bg-circle-outline d-inline-block">
                        <a href="https://www.facebook.com/" class="text-white"><i class="fa fa-2x fa-fw fa-facebook"></i></a>
                     </div>
                     <div class="bg-circle-outline d-inline-block">
                        <a href="https://twitter.com/" class="text-white">
                        <i class="fa fa-2x fa-fw fa-twitter"></i></a>
                     </div>
                     <div class="bg-circle-outline d-inline-block">
                        <a href="https://www.linkedin.com/company/" class="text-white">
                        <i class="fa fa-2x fa-fw fa-linkedin"></i></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div>
       <!-- Bootsrap necesita jQuery para funcionar -->
      <script type="text/javascript" src="js/jquery-3.2.0.min.js"></script>
      <script type="text/javascript" src="js/script.js"></script>
      <!-- Llamamos al JavaScript de Bootstrap-->
      <script src="js/bootstrap.min.js"></script>
    </body>
</html>
