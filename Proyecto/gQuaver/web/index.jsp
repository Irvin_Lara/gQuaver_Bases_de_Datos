<%-- 
    Document   : index.jsp
    Created on : 22-may-2017, 12:11:39
    Author     : tiiko
--%>

<%@page import="ConexionSQLDB.DataBaseConnect"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page language="java" %>
<%@ page import = "java.sql.Connection"%>
<%@ page import = "java.sql.DriverManager"%> 
<%@ page import = "java.sql.ResultSet"%> 
<%@ page import = "java.sql.Statement"%> 

<!DOCTYPE html>
<html>
   <head>
      <title>gQuaver</title>
      <meta charset="utf-8">
      <!-- se vea bien en dispositivos móviles-->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!--Llamamos al archivo CSS -->
      <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
      <link rel="shortcut icon" type="image/x-icon" href="icons/icon.png" />
      <link rel="stylesheet" href="css/style.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
   </head>
   <body>
     <div class="todo">
      <div id="header">
         <nav class="navbar navbar-inverse navbar-fixed-top">
            <img class="icon" src="icons/icon.png" alt="icono guitarra">
            <div class="container-fluid">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a id="gQuaver" class="navbar-brand" href="#">gQuaver</a>
               </div>
               <div>
                  <div class="collapse navbar-collapse" id="myNavbar">
                     <ul class="nav navbar-nav">
                        <li><a class="subir" href="#section1">Subir página</a></li>
                        <li><a class="enlace" href="#section2">Nosotros</a></li>
                        <li><a class="enlace" href="#section4">Contáctanos</a></li>
                        <a href="login.jsp"><li><button type="button" class="btn btn-default navbar-btn" id="myBtn"> ¡Inicia sesión o regístrate!</button></li></a>
                     </ul>
                  </div>
               </div>
            </div>
         </nav>
      </div>
      <div id="main">
         <div id="section1" class="container-fluid">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
               <!-- Indicators -->
               <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                  <li data-target="#myCarousel" data-slide-to="3"></li>
                  <li data-target="#myCarousel" data-slide-to="4"></li>
               </ol>
               <!-- Wrapper for slides -->
               <div class="carousel-inner" role="listbox">
                  <div class="item active">
                     <img src="images/piano.jpg" alt="piano">
                     <div class="carousel-caption">
                        <h3>Tu brillas, nosotros te ayudamos a mejorar</h3>
                        <p>Te ofrecemos los mejores instrumentos musicales.</p>
                     </div>
                  </div>
                  <div class="item">
                     <img src="images/guitarra.jpg" alt="guitarra">
                     <div class="carousel-caption">
                        <h3>Tan únicos como tú</h3>
                        <p>Consigue los mejores accesorios y adornos para tus intrumentos musicales.</p>
                     </div>
                  </div>
                  <div class="item">
                     <img src="images/bateria.jpg" alt="bateria">
                     <div class="carousel-caption">
                        <h3>Que nada te impida llegar lejos</h3>
                        <p>¿Se descompuso tu amplificador? ¿Se rompió alguna baqueta? Aquí puedes conseguir lo que te haga falta.</p>
                     </div>
                  </div>
                  <div class="item">
                     <img src="images/orquesta.jpg" alt="orquesta">
                     <div class="carousel-caption">
                        <h3>Para profesionales y principantes</h3>
                        <p>Además, te ofrecemos los mejores cursos, que se adaptan a tu nivel de aprendizaje.</p>
                     </div>
                  </div>
                  <div class="item">
                     <img src="images/concierto.jpg" alt="concierto">
                     <div class="carousel-caption">
                        <h3>Como las estrellas</h3>
                        <p>Que nada te detenga, aquí conseguirás los productos de la más alta calidad que te convertirán en toda una estrella de la música.</p>
                     </div>
                  </div>
               </div>
               <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
               <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
               <span class="sr-only">Anterior</span>
               </a>
               <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
               <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
               <span class="sr-only">Siguiente</span>
               </a>
            </div>
         </div>
         <div class="contSection">
            <div class="parallax">
               <div id="section2" class="container-fluid">
                  <h1>Calidad, excelencia y profesionalismo</h1>
               </div>
            </div>
            <div id="section3" class="container-fluid">
               <img class="icon2" src="icons/icon.png" alt="icono guitarra">
               <h1>gQuaver</h1>
               <br/>
               <iframe class="ytComercial" src="https://www.youtube.com/embed/FnNOAwo4krw" frameborder="0" allowfullscreen></iframe>
               <h2>¿Quiénes somos?</h2>
               <br/>
               <p>gQuaver, S.A. inicia su fructífera labor en los albores de 1971.
                  Funcionando en principio con distribución local y regional en la comercialización de
                  aparatos y refacciones electrónicas. Amplia su cobertura en la década de los 80s a nivel
                  nacional estableciendo filiales en México, Torreón, Guadalajara, Monterrey y Querétaro.
               </p>
               <p>Nuestra misión es ser los mejores en servicio, atención y precio, contando con un extenso
                  surtido en el ramo de la electrónica y la música, siempre trabajando en equipo y con
                  honestidad para ganarnos el respeto y la lealtad de nuestros clientes.
               </p>
               <br/>
               <p>Pero no sólo vendemos intrumentos musicales, sino también te ofrecemos cursos para que
                  aprendas a ser todo un profesional.
               </p>
            </div>
            <div class="parallaxInstruments">
               <div id="section4" class="container-fluid">
                  <h1>Contáctanos</h1>
               </div>
            </div>
            <div id="section5" class="container-fluid">
               <img src="icons/ubicationIcon.png" alt="icono ubicación">
               <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14003.763052702283!2d-106.1221763!3d28.6614923!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf0fb71e2e39d2851!2sJD+Electronics!5e0!3m2!1ses!2smx!4v1495334818895"
               frameborder="0"  allowfullscreen></iframe>
               <h2>Dirección</h2>
               <p>Plaza Arboledas, Av. Francisco Villa 4907, Int.208, Arboledas, 31110 Chihuahua, Chih.</p>
               <br/>
               <img src="icons/phoneIcon.png" alt="icono teléfono">
               <h2>Teléfono</h2>
               <p> 01 614 417 2888</p>
               <br/>
               <img src="icons/mailIcon.png" alt="icono mail">
               <h2>Correo electrónico</h2>
               <p>info@jdelectronics.com.mx</p>
               <br/>
               <img src="icons/watchIcon.png" alt="icono reloj">
               <h2>Horario de atención</h2>
               <p>Lunes a sábado de 9:00hrs. a 19:30hrs.</p>
               <br/>
               <br/>
            </div>
         </div>
      </div>
      <div id="footer">
      <div class="container-fluid bg-primary py-3">
         <div class="container">
            <div class="row py-3">
               <div class="col-md-9">
                  <p class="text-white">Copyright (c) gQuaver 2017 Todos los derechos reservados.</p>
               </div>
               <div class="col-md-3">
                  <div class="d-inline-block">
                     <div class="bg-circle-outline d-inline-block">
                        <a href="https://www.facebook.com/" class="text-white"><i class="fa fa-2x fa-fw fa-facebook"></i></a>
                     </div>
                     <div class="bg-circle-outline d-inline-block">
                        <a href="https://twitter.com/" class="text-white">
                        <i class="fa fa-2x fa-fw fa-twitter"></i></a>
                     </div>
                     <div class="bg-circle-outline d-inline-block">
                        <a href="https://www.linkedin.com/company/" class="text-white">
                        <i class="fa fa-2x fa-fw fa-linkedin"></i></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div>
  </div>
      <!-- Bootsrap necesita jQuery para funcionar -->
      <script type="text/javascript" src="js/jquery-3.2.0.min.js"></script>
      <script type="text/javascript" src="js/script.js"></script>
      <!-- Llamamos al JavaScript de Bootstrap-->
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>
