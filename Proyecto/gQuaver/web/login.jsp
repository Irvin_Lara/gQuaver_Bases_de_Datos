<%-- 
    Document   : login
    Created on : 28-may-2017, 0:52:17
    Author     : Tiiko Papas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
      <title>Login</title>
      <meta charset="utf-8">
      <!-- se vea bien en dispositivos móviles-->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!--Llamamos al archivo CSS -->
      <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
      <link rel="shortcut icon" type="image/x-icon" href="icons/icon.png" />
      <link rel="stylesheet" href="css/style.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
      <div id="header">
         <nav class="navbar navbar-inverse navbar-fixed-top">
            <img class="icon" src="icons/icon.png" alt="icono guitarra">
            <div class="container-fluid">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  </button>
                  <a id="gQuaver" class="navbar-brand" href="#">gQuaver</a>
               </div>
            </div>
         </nav>
      </div>
        <div id="mainLog">
            <div class="parallaxLog">
                 <div id="section4" class="container-fluid">
                   <b><h1>Iniciar sesión</h1></b>
               </div>
            <div class="login">
                 <!-- Modal -->
               <div class="modal-header" style="padding:35px 50px;">
                  <h4><span class="glyphicon glyphicon-lock"></span> Inicia sesión</h4>
               </div>
               <div class="modal-body" style="padding:40px 50px;">
                   <form class="hola" action="login" method="post">
                     <div class="form-group">
                        <label for="usrname"><span class="glyphicon glyphicon-user"></span> Usuario</label>
                        <input type="text" class="form-control" id="usrname" name="user" placeholder="Ingresa tu usuario">
                     </div>
                     <div class="form-group">
                        <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Contraseña</label>
                        <input type="password" class="form-control" id="psw" name="password"  placeholder="Ingresa contaseña">
                     </div>
                      <input type="submit" value="Iniciar sesión" id="iniNaranja" class="btn btn-success btn-block" />
                  </form>
                  <div class="modal-footer">
                      <a href="index.jsp"><button type="submit" class="btn btn-danger btn-default cancelar" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button></a>
                      <div id="reg" class="registrar btn">¡Regístrate aquí!</div>
                 </div>
               </div>
            </div>
            <br/>
        </div>
      </div>
      <div id="footer">
      <div class="container-fluid bg-primary py-3">
         <div class="container">
            <div class="row py-3">
               <div class="col-md-9">
                  <p class="text-white">Copyright (c) gQuaver 2017 Todos los derechos reservados.</p>
               </div>
               <div class="col-md-3">
                  <div class="d-inline-block">
                     <div class="bg-circle-outline d-inline-block">
                        <a href="https://www.facebook.com/" class="text-white"><i class="fa fa-2x fa-fw fa-facebook"></i></a>
                     </div>
                     <div class="bg-circle-outline d-inline-block">
                        <a href="https://twitter.com/" class="text-white">
                        <i class="fa fa-2x fa-fw fa-twitter"></i></a>
                     </div>
                     <div class="bg-circle-outline d-inline-block">
                        <a href="https://www.linkedin.com/company/" class="text-white">
                        <i class="fa fa-2x fa-fw fa-linkedin"></i></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div>
      <!-- Bootsrap necesita jQuery para funcionar -->
      <script type="text/javascript" src="js/jquery-3.2.0.min.js"></script>
      <script type="text/javascript" src="js/script.js"></script>
      <!-- Llamamos al JavaScript de Bootstrap-->
      <script src="js/bootstrap.min.js"></script>
    </body>
</html>
