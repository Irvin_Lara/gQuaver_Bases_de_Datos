
DROP TABLE instrumentos CASCADE CONSTRAINTS PURGE;
DROP TABLE albumes CASCADE CONSTRAINTS PURGE;
DROP TABLE accesorios CASCADE CONSTRAINTS PURGE;
DROP TABLE compras CASCADE CONSTRAINTS PURGE;
DROP TABLE tarjetas_credito CASCADE CONSTRAINTS PURGE;
DROP TABLE direcciones CASCADE CONSTRAINTS PURGE;
DROP TABLE transportista CASCADE CONSTRAINTS PURGE;
DROP TABLE productos CASCADE CONSTRAINTS PURGE;
DROP TABLE inscripciones CASCADE CONSTRAINTS PURGE;
DROP TABLE cursos CASCADE CONSTRAINTS PURGE;
DROP TABLE clientes CASCADE CONSTRAINTS PURGE;

CREATE TABLE clientes
(
  idCliente            INTEGER NOT NULL,
  nomCliente           VARCHAR2(40) NOT NULL,
	email                VARCHAR2(40) NOT NULL,
	user_name            VARCHAR2(40) NOT NULL,
	pass             VARCHAR2(40) NOT NULL,
CONSTRAINT  clien_idClien_pk PRIMARY KEY (idCliente)
);

CREATE TABLE cursos
(
	idCurso              INTEGER NOT NULL,
	nombreCurso          VARCHAR2(40) NOT NULL,
	descripcion          LONG NOT NULL,
	duracion           VARCHAR2(40) NOT NULL,
CONSTRAINT  cursos_idCurso_pk PRIMARY KEY (idCurso)
);

CREATE TABLE inscripciones
(
	idCliente            INTEGER NOT NULL,
	idCurso              INTEGER NOT NULL,
	fechaIns             DATE NOT NULL,
CONSTRAINT  insc_idCliente_idCurso_pk PRIMARY KEY (idCliente,idCurso),
CONSTRAINT insc_idClciente_fk FOREIGN KEY (idCliente) REFERENCES Clientes (idCliente),
CONSTRAINT insc_idCurso_fk FOREIGN KEY (idCurso) REFERENCES Cursos (idCurso)
);

CREATE TABLE productos
(
	idProducto           INTEGER NOT NULL,
	existencia           INTEGER NOT NULL,
	precio               INTEGER NOT NULL,
CONSTRAINT  produc_idProducto_pk PRIMARY KEY (idProducto)
);

CREATE TABLE transportista
(
	idTransportista      INTEGER NOT NULL ,
	nombreTra            VARCHAR2(60) NOT NULL ,
CONSTRAINT  transp_idTransp_pk PRIMARY KEY (idTransportista)
);

CREATE TABLE direcciones
(
	idCliente            INTEGER NOT NULL,
	idDireccion          INTEGER NOT NULL,
	calle                VARCHAR2(40) NOT NULL,
	colonia              VARCHAR2(40) NOT NULL,
	numeroCasa           VARCHAR2(40) NOT NULL,
	estado               VARCHAR2(40) NOT NULL,
	municipio            VARCHAR2(40) NOT NULL,
CONSTRAINT  direc_idCliente_idDirec_pk PRIMARY KEY (idCliente,idDireccion),
CONSTRAINT direc_idCliente_f FOREIGN KEY (idCliente) REFERENCES Clientes (idCliente)
);

CREATE TABLE tarjetas_credito
(
	idCliente            INTEGER NOT NULL,
	idTarjeta            INTEGER NOT NULL,
	numTarjeta           VARCHAR2(16) NOT NULL,
	codigoSeg            VARCHAR2(3) NOT NULL,
CONSTRAINT  tar_tred_idClie_idTar_pk PRIMARY KEY (idCliente,idTarjeta),
CONSTRAINT tar_idCliente_fk FOREIGN KEY (idCliente) REFERENCES Clientes (idCliente)
);

CREATE TABLE compras
(
	idCompra             INTEGER NOT NULL,
	idCliente            INTEGER NOT NULL,
	idProducto           INTEGER NOT NULL,
	cantidad             INTEGER NOT NULL,
	subTotal             INTEGER NOT NULL,
	iva                  INTEGER NOT NULL,
	total                INTEGER NOT NULL,
	fechaLlegada         DATE NOT NULL,
	idTarjeta            INTEGER NOT NULL,
	idDireccion          INTEGER NOT NULL,
	idTransportista      INTEGER NOT NULL,
CONSTRAINT compras_idCom_idCli_idProd_pk PRIMARY KEY (idCompra,idCliente,idProducto),
CONSTRAINT compras_idCliente_fk FOREIGN KEY (idCliente) REFERENCES Clientes (idCliente),
CONSTRAINT compras_idProducto_fk FOREIGN KEY (idProducto) REFERENCES Productos (idProducto),
CONSTRAINT compras_idTrans_fk FOREIGN KEY (idTransportista) REFERENCES Transportista (idTransportista),
CONSTRAINT compras_idCli_idDirec_fk FOREIGN KEY (idCliente, idDireccion) REFERENCES Direcciones (idCliente, idDireccion),
CONSTRAINT compras_idCli_idTar_fk FOREIGN KEY (idCliente, idTarjeta) REFERENCES Tarjetas_Credito (idCliente, idTarjeta)
);

CREATE TABLE accesorios
(
	idProducto           INTEGER NOT NULL,
	idAccesorio          INTEGER NOT NULL,
	nombreAcc            VARCHAR2(40) NOT NULL,
	descripcionAcc       VARCHAR2(40) NOT NULL,
CONSTRAINT acces_idProdu_idAcces_pk PRIMARY KEY (idProducto,idAccesorio),
CONSTRAINT acces_idProdu_fk FOREIGN KEY (idProducto) REFERENCES Productos (idProducto)
);

CREATE TABLE albumes
(
	idProducto           INTEGER NOT NULL,
	idAlbum              INTEGER NOT NULL,
	nombreAlb            VARCHAR2(40) NOT NULL,
	artista              VARCHAR2(40) NOT NULL,
	genero               VARCHAR2(40) NOT NULL,
	anio                 VARCHAR2(40) NOT NULL,
CONSTRAINT  alb_idProdu_idAlb_pk PRIMARY KEY (idProducto,idAlbum),
CONSTRAINT alb_idProdu_fk FOREIGN KEY (idProducto) REFERENCES Productos (idProducto)
);

CREATE TABLE instrumentos
(
	idProducto           INTEGER NOT NULL,
	idInstrumento        INTEGER NOT NULL,
	nombreIns            VARCHAR2(40) NOT NULL,
	descripcionIns       LONG NOT NULL,
CONSTRAINT  inst_idProdu_idInst_pk PRIMARY KEY (idProducto,idInstrumento),
CONSTRAINT inst_idProducto_fk FOREIGN KEY (idProducto) REFERENCES Productos (idProducto)
);

INSERT INTO clientes VALUES(1, 'Irvin Lara', 'irvin@hotmail.com', 'yoshos', 'abc123');
INSERT INTO clientes VALUES(2, 'Nohem� Gonz�lez', 'nohemi@hotmail.com', 'Tiiko Papas', '123456');

INSERT INTO cursos VALUES(1, 'Piano y teclado', 'Consta de doce libros con m�s de 400 ejercicios y pistas.', '52 lecciones, de dos horas cada una.');
INSERT INTO cursos VALUES(2, 'Armon�a moderna', 'Consta de lecciones de video en HD', '52 lecciones, de dos horas cada una.');
INSERT INTO cursos VALUES(3, 'Guitarra el�ctrica', 'Afinaci�n, acordes y escalas', '52 lecciones, de dos horas cada una.');
INSERT INTO cursos VALUES(4, 'Guitarra el�ctrica', 'Afinaci�n, posiciones y ejercicios de coordinaci�n', '52 lecciones, de dos horas cada una.');
INSERT INTO cursos VALUES(5, 'Bajo el�ctrico', 'Crear l�neas de bajo propias y lectura de cifrado', '52 lecciones, de dos horas cada una.');
INSERT INTO cursos VALUES(6, 'Saxof�n', 'Ejecutar melod�as y patrones r�tmicos en el saxof�n', '52 lecciones, de dos horas cada una.');
INSERT INTO cursos VALUES(7, 'Bater�a', 'Conocimiento del instrumento y ejercicios de coordinaci�n motriz', '52 lecciones, de dos horas cada una.');
INSERT INTO cursos VALUES(8, 'Viol�n', 'Conocer todas las escalas y efectos', '52 lecciones, de dos horas cada una.');
INSERT INTO cursos VALUES(9, 'Canto', 'Ejercicios para desarrollar la voz y t�cnicas vocales esenciales', '52 lecciones, de dos horas cada una.');

INSERT INTO inscripciones VALUES (1, 2, '25-05-2017');
INSERT INTO inscripciones VALUES (2, 3, '26-05-2017');

INSERT INTO transportista VALUES (1, 'DHL');
INSERT INTO transportista VALUES (2, 'Estafeta');

INSERT INTO direcciones VALUES (1, 1, 'Lomas', 'Saucito', '4458', 'Chihuahua', 'Chihuahua');
INSERT INTO direcciones VALUES (2, 2, 'Mayas', 'Infonavit Nacional', '7712', 'Chihuahua', 'Chihuahua');

INSERT INTO tarjetas_credito VALUES (1, 1, '1234567891234567', '123');
INSERT INTO tarjetas_credito VALUES (2, 2, '7894569637418523', '431');

INSERT INTO productos VALUES (1, 3, 5960);
INSERT INTO instrumentos VALUES (1, 1, 'Mitchell', 'Guitarra el�ctrica');
INSERT INTO productos VALUES (2, 1, 33374);
INSERT INTO instrumentos VALUES (2, 2, 'Gibson', 'Guiatrra el�ctrica');
INSERT INTO productos VALUES (3, 3, 1986);
INSERT INTO instrumentos VALUES (3,3, 'Rogue', 'Bajo el�ctrico');
INSERT INTO productos VALUES (4, 4, 7926);
INSERT INTO instrumentos VALUES (4,4, 'Eplphone', 'Bajo el�ctrico');
INSERT INTO productos VALUES (5, 2, 6834);
INSERT INTO instrumentos VALUES (5, 5, 'Mitchell', 'Guitarra ac�stica');
INSERT INTO productos VALUES(6, 3, 914);
INSERT INTO instrumentos VALUES(6, 6, 'Rogue', 'Guitarra ac�stica');
INSERT INTO productos VALUES (7, 2, 5960);
INSERT INTO instrumentos VALUES (7,7, '�lvarez', 'Guitarra electro-ac�stica');
INSERT INTO productos VALUES (8, 1, 2980);
INSERT INTO instrumentos VALUES (8, 8, 'Luna', 'Guitarra electro-ac�stica');
INSERT INTO productos VALUES (9, 1, 1986);
INSERT INTO instrumentos VALUES (9,9, 'SImmons', 'Set de bater�a electr�nica');
INSERT INTO productos VALUES (10, 1, 7946);
INSERT INTO instrumentos VALUES (10, 10, 'Labs Unity', 'Set completo de bater�a');
INSERT INTO productos VALUES (11, 1, 5000);
INSERT INTO instrumentos VALUES (11, 11, 'Lade', 'Saxof�n alto profesional');
INSERT INTO productos VALUES (12, 1, 4745);
INSERT INTO instrumentos VALUES (12, 12, 'Antosvilla', 'Viol�n el�ctrico');

INSERT INTO productos VALUES (13, 10, 150);
INSERT INTO albumes VALUES (13, 1, 'Humanz', 'Gorillaz', 'Indie Rock', '2017');
INSERT INTO productos VALUES (14, 5, 200);
INSERT INTO albumes VALUES (14, 2, 'Once', 'Nightwish', 'Metal Sinf�nico', '2004');
INSERT INTO productos VALUES (15, 15, 150);
INSERT INTO albumes VALUES (15, 3, 'RAM', 'Daft Punk', 'Electr�nica', '2013');
INSERT INTO productos VALUES (16, 10, 130);
INSERT INTO albumes VALUES (16, 4, 'The Beginning', 'Black Eyed Peas', 'Pop', '2010');
INSERT INTO productos VALUES (17, 30, 250);
INSERT INTO albumes VALUES (17, 5, 'Death Magnetic', 'Metallica', 'Thrash Metal', '2008');
INSERT INTO productos VALUES (18, 50, 150);
INSERT INTO albumes VALUES (18, 6, 'Love', 'The Beatles', 'Pop Rock', '2006');
INSERT INTO productos VALUES (19, 5, 120);
INSERT INTO albumes VALUES (19, 7, 'Gaia', 'Mago de OZ', 'Folk Metal', '2003');
INSERT INTO productos VALUES (20, 2, 140);
INSERT INTO albumes VALUES (20, 8, 'Gaia II', 'Mago de OZ', 'Folk Metal', '2005');
INSERT INTO productos VALUES (21, 20, 150);
INSERT INTO albumes VALUES (21, 9, 'Dystopia', 'Megadeth', 'Speed Metal', '2016');
INSERT INTO productos VALUES (22, 10, 150);
INSERT INTO albumes VALUES (22, 10, 'Hypnotize', 'System of a Down', 'Nu Metal', '2005');
INSERT INTO productos VALUES (23, 40, 200);
INSERT INTO albumes VALUES (23, 11, 'Nightmare', 'Avenged Sevenfold', 'Heavy Metal', '2010');
INSERT INTO productos VALUES (24, 40, 200);
INSERT INTO albumes VALUES (24, 12, 'ELV1S', 'Elvis Presley', 'Rock and Roll', '2002');

INSERT INTO productos VALUES (25, 8, 50);
INSERT INTO accesorios VALUES (25, 1, 'Percussion Labs', 'Baquetas');
INSERT INTO productos VALUES (26, 8, 19);
INSERT INTO accesorios VALUES (26,2, 'Clayton Garage Band', 'Paquete de p�as');
INSERT INTO productos VALUES (27, 5, 79);
INSERT INTO accesorios VALUES (27, 3, 'Perris', 'Guitar Strap');
INSERT INTO productos VALUES (28, 29, 397);
INSERT INTO accesorios VALUES (28, 4, 'Gear', 'Bolsa para guitarra ac�stica');
INSERT INTO productos VALUES (29, 5, 318);
INSERT INTO accesorios VALUES (29, 5, 'Gear braided', 'Cable 1/4');
INSERT INTO productos VALUES (30, 2, 396);
INSERT INTO accesorios VALUES (30, 6, 'Lanikal clip', 'Afinador electr�nico de guitarra');
INSERT INTO productos VALUES (31, 1, 7946);
INSERT INTO accesorios VALUES (31, 7, 'Markbass New York', 'Amplificador para bajo');
INSERT INTO productos VALUES (32, 33, 695);
INSERT INTO accesorios VALUES (32, 8, 'Rogue G10', 'Amplificador para guitarra el�ctrica');
INSERT INTO productos VALUES (33, 2, 3450);
INSERT INTO accesorios VALUES (33, 9, 'Electro-Harmonix Silencer', 'Pedal de efectos para guitarra');
INSERT INTO productos VALUES (34, 36, 3973);
INSERT INTO accesorios VALUES (34, 10, 'Markbass Compressore Tube', 'Pedal de efectos para bajo');

INSERT INTO compras VALUES (1, 1, 1, 1, 7926, 16, 7926, '27-05-2017', 1, 1, 1);